//
//  L2CTableView.m
//  L2Cplat
//
//  Created by zhukaiqi on 2017/9/20.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CTableView.h"

@implementation L2CTableView


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style {
    
    if(self = [super initWithFrame:frame style:style]) {
        
        
        [self setUp];
        
        
    }
    
    return self;
    
    
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    if(self = [super initWithFrame:frame]) {
        
        
        [self setUp];
        
        
    }
    
    return self;
    
    
}

- (void)setUp {

    self.estimatedRowHeight = 0;
    self.estimatedSectionHeaderHeight = 0;
    self.estimatedSectionFooterHeight = 0;
    
}

- (void)awakeFromNib {

    [super awakeFromNib];
    
    [self setUp];

}

@end
