//
//  L2CListBaseTableViewController.h
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "L2CListRowCell.h"
#import "L2CHRPersonJobFitSectionHeaderView.h"


@interface L2CListBaseTableViewController : UITableViewController

/** 数组总数 */
@property (nonatomic, strong) NSMutableArray *groups;
/** 附件数组 */
@property (nonatomic,strong)NSArray *filesArray;

/**
 *  返回方法 控制器内如果需要在返回时做工作，只需要重写后，调用super backAction
 
 */
- (void)backAction;

/**
 *  返回方法 控制器内如果需要在返回时做工作，只需要重写后，调用super backAction
 
 */

- (void)backActionToVC:(NSString *)className;



/**
 *  类似备注的方法掉这个方法
 *
 *  @param item 传入item
 *  @param edit 是否需要编辑
 */
- (void)pushContentControllerWith:(L2CListArrowRow *)item edit:(BOOL )edit;

/**
 *  公司多选人页面
 *
 *  @param titleStr 选人标题
 *  @param keyId  userId 或者id
 *  @param item     传入item
 */
- (void)companyMultiPersonControllerWithTitleStr:(NSString *)titleStr keyId:(NSString *)keyId item:(L2CListRow *)item;
/**
 *  公司单选
 *
 *  @param titleStr   选人标题
 *  @param item  传入item
 *  @param keyId  userId 或者id
 *  @param completion 单选完后需要执行的block
 */

- (void)companySinglePersonControllerWithTitleStr:(NSString *)titleStr keyId:(NSString *)keyId item:(L2CListRow *)item completion:(void (^)())completion;

/**
 *  进入选择附件的列表
 *
 *  @param item 传入item
 *  @param edit 编辑状态
 */
- (void)pushFileListControllerWith:(L2CListRow *)item edit:(BOOL)edit;

/**
 进入选择附件的列表

 @param item 传入item
 @param edit 编辑状态
 @param title 页面标题
 @param type 附件type
 */
- (void)pushFileListControllerWith:(L2CListRow *)item edit:(BOOL)edit andTitle:(NSString *)title andType:(NSInteger)type;

/**
 *  部门多选
 *
 *  @param titleStr 标题
 *  @param item     item
 */
- (void)departMutiControllerWithTitleStr:(NSString *)titleStr item:(L2CListRow *)item;

/**
 *  部门单选
 *
 *  @param titleStr 标题
 *  @param item     item
 */
- (void)departSingleControllerWithTitleStr:(NSString *)titleStr item:(L2CListRow *)item;

@end
