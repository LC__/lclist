//
//  L2CListBaseTableViewController.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListBaseTableViewController.h"
#import "ExperienceViewController.h"
#import "CompanyMultiPersonViewController.h"
#import "CompanySinglePersonViewController.h"
#import "DepMultpleSelectViewController.h"
#import "L2CChooseAttachmentController.h"
#import "L2CDepartmentSingleSelectController.h"
@interface L2CListBaseTableViewController ()



@end

@implementation L2CListBaseTableViewController

- (NSMutableArray *)groups{
    if (!_groups) {
        
        _groups = [NSMutableArray array];
    }
    return _groups;
}

- (instancetype)init{
    
    return [super initWithStyle:UITableViewStyleGrouped];
    
}


- (void)viewDidLoad {

    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 0;
    self.tableView.estimatedSectionHeaderHeight = 0;
    self.tableView.estimatedSectionFooterHeight = 0;
    
    self.tableView.backgroundColor = BackGroundColor;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_btn"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    
}

- (void)backAction {
    
    if (self.navigationController.viewControllers.count>1) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [SVPShow disMiss];
}

- (void)backActionToVC:(NSString *)className {
    
    if (self.navigationController.viewControllers.count>1) {
      
        BOOL find = NO;
        
        for (UIViewController *vc in self.navigationController.childViewControllers) {
            
            if ([NSStringFromClass(vc.class) isEqualToString:className]) {
                
                find = YES;
                
                [self.navigationController popToViewController:vc animated:YES];
            }
            
        }
        
        if (find==NO) {
            
            [self backAction];
        }
    }else{
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [SVPShow disMiss];
    
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (self.groups.count==0) {
        return 1;
    }
    return self.groups.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.groups.count==0) {
        return 0;
    }
    
    L2CListSection *group =  self.groups[section];
    
    if (group.isExpand) {
        
        return group.rows.count;
    }
    
    return 0;

   
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (self.groups.count==0) {
        return [[UITableViewCell alloc] init];
    }


    L2CListSection *group = self.groups[indexPath.section];
    
    L2CListRow *row =group.rows[indexPath.row];
    
    L2CListRowCell *cell  = [L2CListRowCell cellWithTableView:tableView andIdentifier:row.identifier];

    
    
    row.indexPath = indexPath;

    cell.row = row;
    
    if (row.isShowSeparator) {
        cell.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    }else {
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, kWidth);
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.groups.count==0) {
        
        return;
    }
    // 1.取出组模型
    L2CListSection *group =  self.groups[indexPath.section];
    // 2.取出行模型
    L2CListRow *row =  group.rows[indexPath.row];
    

    if ([row isKindOfClass:[L2CListArrowRow class]]||[row isKindOfClass:[L2CListDefultRow class]]||[row isKindOfClass:[L2CListContentViewRow class]]||[row isKindOfClass:[L2CListCustomRow class]]) {
        
        if (row.clickRowAction) {
            
            row.clickRowAction(indexPath);
        }
    }
    
   else if ([row isKindOfClass:[L2CListIconRow class]]) {
        
        if (row.clickRowAction) {
            
            L2CListIconRow *iconRow = (L2CListIconRow *)row;
            if (iconRow.isClickRow) {
                
                 row.clickRowAction(iconRow);
            }
           
        }
    }
    
    if (!([row isKindOfClass:[L2CListTextFieldRow class]]||[row isKindOfClass:[L2CListTextViewRow class]])) {
        
      
        [KeyWindow endEditing:YES];

        
    }
    

}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if (self.groups.count==0) {
        
        return nil;
    }
    // 1.取出组模型
    L2CListSection *group = self.groups[section];
    
    return group.headerTitle;
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    
    if (self.groups.count==0) {
        
        return nil;
    }
    // 1.取出组模型
    L2CListSection *group = self.groups[section];
    
    return group.footTitle;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    if (self.groups.count==0) {
        
        return 57;
    }
    L2CListSection *group = self.groups[indexPath.section];
    
    L2CListRow *row = group.rows[indexPath.row];

    
    return row.rowHeight?row.rowHeight:57;

}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    if (self.groups.count==0) {
        
        return 0.01;
    }
    L2CListSection *group = self.groups[section];
    
    return group.headerHeight?group.headerHeight:0.01;

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {

    if (self.groups.count==0) {
        
        return 0.01;
    }
    L2CListSection *group = self.groups[section];
    
    return group.footerHeight?group.footerHeight:0.01;


}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (self.groups.count ==0) {
        
        return [super tableView:tableView viewForHeaderInSection:section];
    }
    L2CListSection *group = self.groups[section];
    
    if (group.viewForHeader) {
        
        return group.viewForHeader;
    }
    
    return [super tableView:tableView viewForHeaderInSection:section];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if (self.groups.count ==0) {
        
       return [super tableView:tableView viewForFooterInSection:section];
    }
    L2CListSection *group = self.groups[section];
    
    if (group.viewForFooter) {
        
        return group.viewForFooter;
    }
    
    return [super tableView:tableView viewForFooterInSection:section];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {


    [KeyWindow endEditing:YES];

}

- (void)dealloc {
    [KNotification removeObserver:self];
   
}

/**
 *  类似备注的方法掉这个方法
 *
 *  @param item 传入item
 *  @param edit 是否需要编辑
 */
- (void)pushContentControllerWith:(L2CListArrowRow *)item edit:(BOOL )edit {
    
    ExperienceViewController *vc = [[ExperienceViewController alloc] init];
    
    vc.ploText = [NSString stringWithFormat:@"%@%@",L2CLocalized(@"public.Please enter", @"Public",@"请输入"),item.title];
    
    vc.saveText = item.subTitle;
    
    vc.vcTitle = item.title;
    
    vc.textLength = item.length;
    
    vc.block = ^(NSString *backstr) {
        
        item.subTitle = backstr;
        
        tableViewLoadSuccess(self.tableView);
    };
    
    vc.isEnditor = edit;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

/**
 *  公司多选人页面
 *
 *  @param titleStr 选人标题
 *  @param item     传入item
 */

- (void)companyMultiPersonControllerWithTitleStr:(NSString *)titleStr keyId:(NSString *)keyId  item:(L2CListRow *)item {

    CompanyMultiPersonViewController *vc = [[CompanyMultiPersonViewController alloc]init];
    vc.titleStr = titleStr;
    
    vc.personArray = item.personArray;
    
    vc.backBlock = ^(NSMutableArray *arr){
        
        item.personArray = arr;
        
        [self backAction];
        
        if (arr.count) {
            
            NSMutableString *personId = [NSMutableString string];
            NSMutableString *personName = [NSMutableString string];
            
            for (int i=0 ;i<arr.count;i++) {
                
                [personId appendString:[NSString stringWithFormat:@"%@,",arr[i][keyId]]];
                
                [personName appendString:[NSString stringWithFormat:@"%@,",arr[i][@"text"]]];
                
            }
            if ([item isKindOfClass:[L2CListSelectBtnAndArrowRow class]]) {
                
                L2CListSelectBtnAndArrowRow *item1 = (L2CListSelectBtnAndArrowRow *)item;
                
                item1.subTitle2 = [personName substringToIndex:personName.length-1];
                item1.subTitle3 = [personId substringToIndex:personId.length-1];

            }
            else {
            
                item.subTitle = [personName substringToIndex:personName.length-1];
                item.subTitle2 = [personId substringToIndex:personId.length-1];

            
            }
            
            
        }
        
        tableViewLoadSuccess(self.tableView);
        
    };
    
    [self.navigationController pushViewController:vc animated:YES];

}
/**
 *  公司单选
 *
 *  @param titleStr   选人标题
 *  @param item  传入item
 *  @param completion 单选完后需要执行的block
 */

- (void)companySinglePersonControllerWithTitleStr:(NSString *)titleStr keyId:(NSString *)keyId item:(L2CListRow *)item  completion:(void (^)())completion{

    CompanySinglePersonViewController *pVC = [[CompanySinglePersonViewController alloc] init];
    
    pVC.titleStr = titleStr;
    
    pVC.backBlock = ^(NSDictionary *dict) {
        
        if ([item isKindOfClass:[L2CListSelectBtnAndArrowRow class]]){
            
            L2CListSelectBtnAndArrowRow *item1 = (L2CListSelectBtnAndArrowRow *)item;
            
            item1.subTitle2 = dict[@"text"];
            
            item1.subTitle3 = dict[keyId];
            
        }
        else {
            
            item.subTitle = dict[@"text"] ;
            
            item.subTitle2 = dict[keyId];
            
        }
      
        tableViewLoadSuccess(self.tableView);
        
        if (completion) {
            
            completion();
        }
    };
    
    [self.navigationController pushViewController:pVC animated:YES];

}
/**
 *  部门多选
 *
 *  @param titleStr 标题
 *  @param item     item
 */
- (void)departMutiControllerWithTitleStr:(NSString *)titleStr item:(L2CListRow *)item {

    DepMultpleSelectViewController *depVC = [[DepMultpleSelectViewController alloc] init];
    
    depVC.titleStr = titleStr;
    
    depVC.personArray = item.personArray;
    
    depVC.backBlock = ^(NSMutableArray *array) {
        
        item.personArray = array;
        
        [self backAction];
        
        if (array.count) {
            
            NSMutableString *depId = [NSMutableString string];
            NSMutableString *depName = [NSMutableString string];
            
            for (int i=0 ;i<array.count;i++) {
                
                [depId appendString:[NSString stringWithFormat:@"%@,",array[i][@"id"]]];
                
                [depName appendString:[NSString stringWithFormat:@"%@,",[array[i][@"text"] componentsSeparatedByString:@"("].firstObject]];
                
            }
            
            if ([item isKindOfClass:[L2CListSelectBtnAndArrowRow class]]) {
                
                L2CListSelectBtnAndArrowRow *item1 = (L2CListSelectBtnAndArrowRow *)item;
                
                item1.subTitle2 = [depName substringToIndex:depName.length-1];
                item1.subTitle3 = [depId substringToIndex:depId.length-1];   
            }
            else {
                
                item.subTitle = [depName substringToIndex:depName.length-1];
                
                item.subTitle2 = [depId substringToIndex:depId.length-1];

            }

            
        }
        tableViewLoadSuccess(self.tableView);
        
    };
    
    
    [self.navigationController pushViewController:depVC animated:YES];
}

/**
 *  部门单选
 *
 *  @param titleStr 标题
 *  @param item     item
 */
- (void)departSingleControllerWithTitleStr:(NSString *)titleStr item:(L2CListRow *)item {
    
    
    L2CDepartmentSingleSelectController *depVC = [[L2CDepartmentSingleSelectController alloc] init];
    
    depVC.titleStr = titleStr;

    depVC.SelectBlock = ^(NSDictionary *dict) {
        
        [self backAction];
        
            if ([item isKindOfClass:[L2CListSelectBtnAndArrowRow class]]) {
                
                L2CListSelectBtnAndArrowRow *item1 = (L2CListSelectBtnAndArrowRow *)item;
                
                item1.subTitle2 = dict[@"text"];
                item1.subTitle3 = ToSafeString(dict[@"id"]);
                
            }
            else {
                
                item.subTitle = dict[@"text"];
                
                item.subTitle2 = ToSafeString(dict[@"id"]);
                
            }
        tableViewLoadSuccess(self.tableView);
        
    };
    
    
    [self.navigationController pushViewController:depVC animated:YES];
    
}

/**
 *  进入选择附件的列表
 *
 *  @param item 传入item
 *  @param edit 编辑状态
 */
- (void)pushFileListControllerWith:(L2CListRow *)item edit:(BOOL)edit{
    
    L2CWeakSelf(self);
    
    L2CChooseAttachmentController *attachVC = [[L2CChooseAttachmentController alloc] initWithStyle:(UITableViewStyleGrouped)];
    
    attachVC.isCanAdd = edit;
    
    attachVC.isCanEdit = edit;
    
    //已有的附件 模型转换
    NSMutableArray *attachEditArr = [NSMutableArray array];
    
    for (NSDictionary *dict in self.filesArray) {
        L2CAttachMentModel *eModel = [[L2CAttachMentModel alloc] init];
        eModel.attachmentName = dict[@"fileName"];
        eModel.attachmentUrl_sever = dict[@"filePath"];
        eModel.attachmentType = dict[@"fileType"];
        eModel.attachmentSize = dict[@"fileSize"];
        eModel.createTime = dict[@"uploadTime"];
        eModel.createPerson = dict[@"uploadUserName"];
        [attachEditArr addObject:eModel];
    }
    attachVC.attachMentArr = attachEditArr;
    
    attachVC.saveAttachBlock = ^(NSMutableArray<L2CAttachMentModel *> *saveAttachmantArr) {
        
        NSMutableArray *newAttachArr = [NSMutableArray array];
        
        for (L2CAttachMentModel *saveAttach in saveAttachmantArr) {
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            
            [dict setValue:saveAttach.attachmentName forKey:@"fileName"];
            
            [dict setValue:saveAttach.attachmentUrl_sever forKey:@"filePath"];
            
            [dict setValue:saveAttach.attachmentType forKey:@"fileType"];
            
            [dict setValue:saveAttach.createTime forKey:@"uploadTime"];
            
            [dict setValue:saveAttach.createPerson forKey:@"uploadUserName"];
            
            NSString *fileSize = saveAttach.attachmentSize;
            
            fileSize = [fileSize substringToIndex:fileSize.length - 2];
            
            fileSize = [NSString stringWithFormat:@"%d",(int)([fileSize floatValue] * 1024)] ;
            
            [dict setValue:fileSize forKey:@"fileSize"];
            
            [newAttachArr addObject:dict];
        }
        
        weakself.filesArray = newAttachArr;
        
        if (item) {
            item.subTitle = [NSString stringWithFormat:@"%ld",newAttachArr.count];
        }
        
        tableViewLoadSuccess(weakself.tableView);
        
    };
    
    [self.navigationController pushViewController:attachVC animated:YES];
    
}

/**
 *  进入选择附件的列表
 *
 *  @param item 传入item
 *  @param edit 编辑状态
 */
- (void)pushFileListControllerWith:(L2CListRow *)item edit:(BOOL)edit andTitle:(NSString *)title andType:(NSInteger)type{
    
    L2CWeakSelf(self);
    
    L2CChooseAttachmentController *attachVC = [[L2CChooseAttachmentController alloc] initWithStyle:(UITableViewStyleGrouped)];
    
    attachVC.isCanAdd = edit;
    attachVC.title = title;
    attachVC.attachChooseType = type;
    attachVC.isCanEdit = edit;
    
    //已有的附件 模型转换
    NSMutableArray *attachEditArr = [NSMutableArray array];
    
    for (NSDictionary *dict in self.filesArray) {
        L2CAttachMentModel *eModel = [[L2CAttachMentModel alloc] init];
        eModel.attachmentName = dict[@"fileName"];
        eModel.attachmentUrl_sever = dict[@"filePath"];
        eModel.attachmentType = dict[@"fileType"];
        eModel.attachmentSize = dict[@"fileSize"];
        eModel.createTime = dict[@"uploadTime"];
        eModel.createPerson = dict[@"uploadUserName"];
        [attachEditArr addObject:eModel];
    }
    attachVC.attachMentArr = attachEditArr;
    
    attachVC.saveAttachBlock = ^(NSMutableArray<L2CAttachMentModel *> *saveAttachmantArr) {
        
        NSMutableArray *newAttachArr = [NSMutableArray array];
        
        for (L2CAttachMentModel *saveAttach in saveAttachmantArr) {
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            
            [dict setValue:saveAttach.attachmentName forKey:@"fileName"];
            
            [dict setValue:saveAttach.attachmentUrl_sever forKey:@"filePath"];
            
            [dict setValue:saveAttach.attachmentType forKey:@"fileType"];
            
            [dict setValue:saveAttach.createTime forKey:@"uploadTime"];
            
            [dict setValue:saveAttach.createPerson forKey:@"uploadUserName"];
            
            NSString *fileSize = saveAttach.attachmentSize;
            
            fileSize = [fileSize substringToIndex:fileSize.length - 2];
            
            fileSize = [NSString stringWithFormat:@"%d",(int)([fileSize floatValue] * 1024)] ;
            
            [dict setValue:fileSize forKey:@"fileSize"];
            
            [newAttachArr addObject:dict];
        }
        
        weakself.filesArray = newAttachArr;
        
        item.subTitle = [NSString stringWithFormat:@"%ld",newAttachArr.count];
        
        tableViewLoadSuccess(weakself.tableView);
        
    };
    
    [self.navigationController pushViewController:attachVC animated:YES];
    
}

@end
