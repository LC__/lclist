//
//  L2CListDemoController.h
//  L2Cplat
//
//  Created by zhukaiqi on 2017/4/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListBaseTableViewController.h"

/**
 这个 Controller 不用更换
 */
@interface L2CListDemoController : L2CListBaseTableViewController

@end
