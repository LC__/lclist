//
//  L2CListDemoController.m
//  L2Cplat
//
//  Created by zhukaiqi on 2017/4/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListDemoController.h"

@interface L2CListDemoController ()

@end

@implementation L2CListDemoController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setupGrup0];
    
    [self setupGrup1];
    
}
// 第0组
- (void)setupGrup0{
    // 创建行模型
    __weak typeof(self) weakSelf = self;
    
    L2CListDefultRow *item0 = [L2CListDefultRow itemWithTitle:@"名称" mustTag:NO];
    item0.subTitle = @"北京到西安出差";
    
    L2CListArrowRow *item1 = [L2CListArrowRow itemWithTitle:@"出发时间" mustTag:YES];
    item1.clickRowAction = ^(NSIndexPath *indexPath){
        
        
        NSLog(@"我点击啦");
        
    };
    
    L2CListSwitchRow *item2 = [L2CListSwitchRow itemWithTitle:@"是否全天事件" mustTag:YES];
    item2.on = NO;
    item2.clickRowAction = ^(NSNumber *isOn) {
        
        L2CListSection *section = weakSelf.groups[1];
        
        L2CListArrowRow *arrowRow = section.rows[1];
        
        if ([isOn boolValue]) {
            
            arrowRow.subTitle = [arrowRow.noAllDayTime substringToIndex:10];
            
        }
        else {
            
            arrowRow.subTitle = arrowRow.noAllDayTime;
            
        }
        
        [weakSelf.tableView reloadRowsAtIndexPaths:@[  [NSIndexPath indexPathForRow:1 inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
        
    };
    
    L2CListTextFieldRow *item3 = [L2CListTextFieldRow itemWithTitle:@"计划名称" mustTag:YES];
    
    l2CListSelectBtnRow *item4 = [l2CListSelectBtnRow itemWithTitle:@"优先级" mustTag:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        item4.array = @[@{@"id":@"1",@"value":@"火车"},
                        @{@"id":@"2",@"value":@"汽车"},
                        @{@"id":@"3",@"value":@"飞机"},
                        @{@"id":@"4",@"value":@"大炮"},
                        @{@"id":@"5",@"value":@"火箭"}
                        ];
        
        [self .tableView reloadData];
        
    });
    
    item4.rowHeight = 57+2*(34+5);
    
    L2CListSelectBtnAndArrowRow *item5 = [L2CListSelectBtnAndArrowRow itemWithTitle:@"相关业务" mustTag:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        item5.array = @[@{@"id":@"1",@"value":@"线索"},
                        @{@"id":@"2",@"value":@"机会"},
                        ];
        
        [weakSelf.tableView reloadData];
        
    });
    
    item5.clickTitleBtnAction = ^(NSNumber *expand) {
        
        [weakSelf.tableView reloadData];
        
    };
    
    item5.clickRowAction = ^(NSString *btnId) {
        
        NSLog(@"%@",btnId);
    };
    
    
    
    L2CListSection *section = [L2CListSection sectionWithRows:[@[item0,item1,item2,item3,item4,item5] mutableCopy]];
    
    [self.groups addObject:section];
    
}
- (void)setupGrup1{
    // 创建行模型
    
    __weak typeof(self) weakSelf = self;
    
    L2CListDefultRow *item0 = [L2CListDefultRow itemWithTitle:@"名称" mustTag:NO];
    item0.subTitle = @"北京到西安出差";
    
    L2CListArrowRow *item1 = [L2CListArrowRow itemWithTitle:@"出发时间" mustTag:YES];
    item1.subTitle = @"2010-12-7 12:00";
    item1.noAllDayTime = @"2010-12-7 12:00";
    
    item1.clickRowAction = ^(NSIndexPath *indexPath) {
        
        L2CListSection *section = weakSelf.groups[1];
        
        l2CListSelectBtnRow *textFieldRow = section.rows.lastObject;
        
        NSLog(@"%@",textFieldRow.saveDict);
        
    };
    
    L2CListDefultRow *item2 = [L2CListDefultRow itemWithTitle:@"名称" mustTag:YES];
    item2.subTitle = @"北京到西安出差";
    
    L2CListDefultRow *item3 = [L2CListDefultRow itemWithTitle:@"名称" mustTag:YES];
    item3.subTitle = @"北京到西安出差";
    
    L2CListDefultRow *item4 = [L2CListDefultRow itemWithTitle:@"名称" mustTag:YES];
    
    item4.subTitle = @"北京到西安出差";
    
    L2CListArrowRow *item5 = [L2CListArrowRow itemWithTitle:@"出发时间" mustTag:YES];
    item5.subTitle = @"2010-12-7 12:00";
    
    
    l2CListSelectBtnRow *item6 = [l2CListSelectBtnRow itemWithTitle:@"优先级" mustTag:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        item6.array = @[@{@"id":@"1",@"value":@"火车"},
                        @{@"id":@"2",@"value":@"汽车"},
                        @{@"id":@"3",@"value":@"飞机"},
                        @{@"id":@"4",@"value":@"大炮"},
                        @{@"id":@"5",@"value":@"火箭"}
                        ];
        
        [self .tableView reloadData];
        
    });
    
    item6.rowHeight = 57+2*(34+5);
    
    L2CListSection *section = [L2CListSection sectionWithRows:[@[item0,item1,item2,item3,item4,item5,item6] mutableCopy]];
    
    section.headerHeight = 20;
    
    [self.groups addObject:section];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
