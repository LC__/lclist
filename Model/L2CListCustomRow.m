//
//  L2CListCustomRow.m
//  L2Cplat
//
//  Created by zhukaiqi on 2017/8/8.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListCustomRow.h"

@implementation L2CListCustomRow


- (NSString *)identifier {

    if (!self.clazz) {
        
        return [super identifier];
    }
    return NSStringFromClass(self.clazz);
}

@end
