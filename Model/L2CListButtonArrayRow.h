//
//  L2CListButtonArrayRow.h
//  L2Cplat
//
//  Created by zhukaiqi on 2017/8/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListButtonArrayRow : L2CListRow

/** item信息数组 @{@"image":@"xxx",@"title":@"sss",@"badge":@"0"} */
@property (nonatomic,strong)NSArray *imagesArray;

/** 是否显示上分割线  默认不显示 */
@property (nonatomic,assign,getter=isTopSeparatorLine)BOOL topSeparatorLine;

/** 是否显示下分割线  默认不显示 */
@property (nonatomic,assign,getter=isBottomSeparatorLine)BOOL bottomSeparatorLine;

/** 点击这一行要做的事情 */
@property (nonatomic, copy) void(^clickButtonIndexAction)(NSInteger index, NSString *title);

@end
