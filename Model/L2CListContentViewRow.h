//
//  L2CListContentViewRow.h
//  L2Cplat
//
//  Created by feaonline on 2017/5/10.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListContentViewRow : L2CListRow

/**
 内容行数
 */
@property (nonatomic, assign) NSUInteger linesNum;

/** 字体 */
@property (nonatomic,strong) UIFont *ctitleFont;

/** 标题高度 */
@property(nonatomic, assign) CGFloat titleH;

/** 内容高度 */
@property(nonatomic, assign) CGFloat contentH;
#pragma mark -
//显示优化参数不用计算
/** 内容顶部 */
@property(nonatomic, assign) CGFloat conT;

/** 内容底部 */
@property(nonatomic, assign) CGFloat conB;
@end
