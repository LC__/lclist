//
//  l2CListSelectBtnRow.h
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface l2CListSelectBtnRow : L2CListRow

/** 按钮数组 */
@property (nonatomic,strong)NSArray *array;

/** 是否多选 */
@property (nonatomic,assign,getter=isMultiple)BOOL multiple;

/** 占位字符 */
@property (nonatomic,copy)NSString *placeHolder;

/** 键盘类型 */
@property (nonatomic,assign)UIKeyboardType keyboardType;

/** 是否可以输入 默认不可以输入*/
@property (nonatomic,assign,getter=isInput)BOOL input;

/** 子标题2 */
@property (nonatomic,copy)NSString *subTitle3;

/** 子标题3 */
@property (nonatomic,copy)NSString *subTitle4;

/** 子标题4 */
@property (nonatomic,copy)NSString *subTitle5;

/** 是否显示自定义日期控件*/
@property (nonatomic,assign,getter=isHiddenDate)BOOL hiddenDate;

/** 第一个占位字符 */
@property (nonatomic,copy)NSString *firstPlaceHolder;

/** 第二个占位字符 */
@property (nonatomic,copy)NSString *lastPlaceHolder;

/** 文本输入时执行的block */
@property (nonatomic,copy)void(^textInPutAfterBlock)(id object) ;

/** 点击自定义的时间控件执行的block */
@property (nonatomic,copy)void(^clickCustomDateView)(id object);

@end
