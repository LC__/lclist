//
//  L2CListSection.h
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface L2CListSection : NSObject

/** 头部标题 */
@property (nonatomic, copy) NSString *headerTitle;

/** 尾部标题 */
@property (nonatomic, copy) NSString *footTitle;

/** 行模型数组 */
@property (nonatomic, strong) NSMutableArray *rows;

/** 区头高度 有字：30  无字：18*/
@property (nonatomic,assign)CGFloat headerHeight;

/** 区尾高度 */
@property (nonatomic,assign)CGFloat footerHeight;

/** 区头视图 */
@property (nonatomic,strong)UIView *viewForHeader;

/** 区尾视图 */
@property (nonatomic,strong)UIView *viewForFooter;


/** 分区展开或者合起 */
@property (nonatomic,assign,getter=isExpand)BOOL expand;


+ (instancetype)sectionWithRows:(NSMutableArray *)rows;

@end
