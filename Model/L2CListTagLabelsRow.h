//
//  L2CListTagLabelsRow.h
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/8/21.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListTagLabelsRow : L2CListRow
/**
 标题后附加的 标签
 
 默认值
 height ： 21
 
 @code
 @{
 @"tagTitle"        :   (NSString)@"标签标题",
 @"tagTextColor"    :   (UIColor) @"字以及边框颜色",
 @"tagBgColor"      :   (UIColor) @"标签背景颜色",
 }
 @endcode
 */
@property (nonatomic, strong) NSArray *tagInfoArr;

/**
 行数限制
 @warning 默认 0 - 无限制
 */
@property(nonatomic, assign) NSUInteger rowNum;

/**
 是否打开
 */
@property(nonatomic, assign) BOOL isExpand;

/** 点击更多按钮要做的事情 */
@property (nonatomic, copy) void(^moreRowAction)(id object);
@end
