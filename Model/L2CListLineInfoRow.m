//
//  L2CListLineInfoRow.m
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/8/16.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListLineInfoRow.h"

@implementation L2CListLineInfoRow

- (UIColor *)lineColor{
    if (!_lineColor) {
        _lineColor = CustomBlueColor;
    }
    return _lineColor;
}

- (UIColor *)itemCColor{
    if (!_itemCColor) {
        _itemCColor = CustomTextShowColor;
    }
    return _itemCColor;
}

- (UIColor *)itemTColor{
    if (!_itemTColor) {
        _itemTColor = [UIColor blackColor];
    }
    return _itemTColor;
}

- (void)setLineInfoArr:(NSMutableArray *)lineInfoArr{
    NSMutableArray *arrTemp = @[].mutableCopy;
    for (NSDictionary *info in lineInfoArr) {
        NSMutableDictionary *infoM = [NSMutableDictionary dictionaryWithDictionary:info];
        [infoM setObject:@(0) forKey:@"height"];
        [arrTemp addObject:infoM];
    }
    _lineInfoArr = arrTemp;
}

- (void)setTagInfoArr:(NSArray *)tagInfoArr{
    NSMutableArray *arrTemp = @[].mutableCopy;
    for (NSDictionary *info in tagInfoArr) {
        NSMutableDictionary *infoM = [NSMutableDictionary dictionaryWithDictionary:info];
        [infoM setObject:@(0) forKey:@"width"];
        [arrTemp addObject:infoM];
    }
    _tagInfoArr = arrTemp;
}

@end
