//
//  L2CListEchartsListRow.h
//  L2Cplat
//
//  Created by L2Cplat on 2018/3/2.
//  Copyright © 2018年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListEchartsListRow : L2CListRow
/** 图形配置 */
@property (nonatomic,strong)PYOption *chartOption;

/** 是否刷新布局 */
@property (nonatomic,assign) BOOL isRefreshFrame;

/** 图形的布局 */
@property (nonatomic,assign) CGSize chartFrame;

/** webView加载完毕执行的block */
@property (nonatomic,copy)void(^chartsListRowDidFinishLoad)(WKEchartsView *webView);
@end
