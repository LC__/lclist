//
//  L2CListSelectBtnAndArrowRow.h
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListSelectBtnAndArrowRow : L2CListRow

/** 按钮数组 */
@property (nonatomic,strong)NSArray *array;

/** 是否展开 */
@property (nonatomic,assign,getter=isExpand)BOOL expand;

/** subtitle3 */
@property (nonatomic,copy)NSString *subTitle3;

/** 点击这一行要做的事情 */
@property (nonatomic, copy) void(^clickTitleBtnAction)(id object);

/** 点击下方按钮执行的操作 */
@property (nonatomic,copy)void(^clickSubBtnAction)(id object);

/** 是否关闭交互 默认开启 */
@property (nonatomic,assign,getter=isMutual)BOOL mutual;

/**
 返回 第一个 被选中的按钮的标题
 */
@property (nonatomic, copy) NSString *curTitle;



@end
