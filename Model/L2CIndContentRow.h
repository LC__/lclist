//
//  L2CIndContentRow.h
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/11/8.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CIndContentRow : L2CListRow

/** 右侧按钮图片 */
@property (nonatomic, copy) NSString *btnImg;

/** 右侧按钮图片 */
@property (nonatomic, copy) NSString *btnTitle;

/** 是否显示前面的线 默认不显示 此属性确定了cell的两种样式 */
@property(nonatomic, assign) BOOL isShowLine;

/** 按钮事件 */
@property (nonatomic, copy) void(^btnBlock)(L2CIndContentRow* row);
@end
