//
//  L2CListIconRow.h
//  L2Cplat
//
//  Created by zhukaiqi on 2017/4/19.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListIconRow : L2CListRow

/** 头像地址 */
@property (nonatomic,copy)NSString *imageUrl;

/** 是否隐藏箭头 */
@property (nonatomic,assign,getter=isHideArrow)BOOL hideArrow;

/** selectedImage */
@property (nonatomic,strong)UIImage *selectedImage;

/** 点击这一行要做的事情 */
@property (nonatomic, copy) void(^clickIconViewAction)(id object);

/** 头像是否可以点击 */
@property (nonatomic,assign,getter=isClickIcon)BOOL clickIcon;

/** 是否选中 */
@property (nonatomic,assign,getter=isSelected)BOOL selected;

/** 是否可以点击行 默认可以YES */
@property (nonatomic,assign,getter=isClickRow)BOOL clickRow;

/**
 提示语句
 */
@property (nonatomic, copy) NSString *ploText;

/** 红点提醒 */
@property (nonatomic,copy)NSString *badgeCount;


@end
