//
//  L2CListSection.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListSection.h"

@implementation L2CListSection

- (instancetype)init {
    
    if (self = [super init]) {
        
        
        _expand = YES;
    }
    
    return self;
    
}

+ (instancetype)sectionWithRows:(NSMutableArray *)rows{
    
    L2CListSection *section = [[self alloc] init];
    
    section.rows = rows;
    
    section.expand = YES;
 
    return section;
}

- (NSMutableArray *)rows {

    if (!_rows) {
        
        _rows = [NSMutableArray array];
    }
    
    return _rows;

}
@end
