//
//  L2CListButtonArrayRow.m
//  L2Cplat
//
//  Created by zhukaiqi on 2017/8/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListButtonArrayRow.h"

@implementation L2CListButtonArrayRow



- (void)setImagesArray:(NSArray *)imagesArray {

    _imagesArray = imagesArray;
    
    self.rowHeight = ceilf(kWidth/4.0);
 
}


@end
