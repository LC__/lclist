//
//  L2CListContentViewRow.m
//  L2Cplat
//
//  Created by feaonline on 2017/5/10.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListContentViewRow.h"
#import "NSString+StrSize.h"

@implementation L2CListContentViewRow

- (UIFont *)ctitleFont{
    if (!_ctitleFont) {
        _ctitleFont = [UIFont systemFontOfSize:16];
    }
    return _ctitleFont;
}

- (CGFloat)titleH{
    
    CGFloat tH = [self.title sizeWithFont:self.ctitleFont and:CGSizeMake(kWidth - 30, MAXFLOAT)].height + 5;
    if (!self.title.length) {
        tH = 0;
    }
    
    if (tH < 57) {
        if (!self.subTitle.length) {
            tH = 57;
        }else{
            tH += 10;
        }
    }
    

    
    return tH;
}

- (CGFloat)contentH{
    
    if (!self.subTitle.length) {
        _contentH = 0;
        self.conT = 0;
        self.conB = 0;
        return _contentH;
    }
    
    CGFloat height = ceil([self.subTitle sizeWithFont:[UIFont systemFontOfSize:14.0] and:CGSizeMake(kWidth-42, MAXFLOAT)].height) + 3;
    self.conT = 0;
    self.conB = 10;
    return height;
}

@end
