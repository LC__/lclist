//
//  L2CListSelectBtnAndArrowRow.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListSelectBtnAndArrowRow.h"

@implementation L2CListSelectBtnAndArrowRow

- (instancetype)init {

    if (self = [super init]) {
        
        _mutual = YES;
    }
    
    return self;

}

- (void)setExpand:(BOOL)expand {

    _expand = expand;
    
    if (self.isExpand) {
        
        CGFloat row = ceilf(self.array.count  / 4.0) ;
        
        self.rowHeight = 57 + row * (34 + 5);
        
    }
    else {
        
        self.rowHeight = 57;
    }

}

- (NSString *)subTitle3 {
  
    if (!_subTitle3) {
        
        _subTitle3 = @"";
    }
    
    return _subTitle3;

}

- (NSString *)curTitle{
    for (NSDictionary *infoDic in self.array) {
        if ([infoDic[@"id"] isEqualToString:self.subTitle]) {
            return infoDic[@"value"];
        }
    }
    return @"";
}
@end
