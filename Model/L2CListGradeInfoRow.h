//
//  L2CListGradeInfoRow.h
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/8/16.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"


/**
 评分信息 ListRowModel
 */
@interface L2CListGradeInfoRow : L2CListRow


/**
 评分信息数组
 @code
 @{
    @"title"    :   @"标题",
    @"subTitle" :   @"副标题",
    @"bgColor"  :   @"分数颜色",
    @"persent"  :   @"百分比数", //50.5% 传入 50.5
 }
 @endcode
 */
@property (nonatomic, strong) NSArray *gradeInfoArr;

@end
