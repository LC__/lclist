//
//  L2CListSalaryRow.h
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/11/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListSalaryRow : L2CListRow
/** 薪水总计 */
@property (nonatomic, copy) NSString *totalNum;

/** 薪水ItemArr */
@property (nonatomic, strong) NSMutableArray *itemArr;

/** 是否显示前面的线 默认不显示 此属性确定了cell的两种样式 */
@property(nonatomic, assign) BOOL isShowLine;

@end
