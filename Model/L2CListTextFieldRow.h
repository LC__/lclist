//
//  L2CListTextFieldRow.h
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListTextFieldRow : L2CListRow

/** 占位字符 */
@property (nonatomic,copy)NSString *placeHolder;

/** 键盘类型 */
@property (nonatomic,assign)UIKeyboardType keyboardType;

/** 是否可以输入 默认可以输入 */
@property (nonatomic,assign,getter=isInput) BOOL input;

/** 是否显示右边 btn  默认NO 不显示*/
@property (nonatomic,assign,getter=isShowBtn) BOOL showBtn;

/** 右边btn图片名称 */
@property (nonatomic,copy)NSString *btnImageName;

/** 右边btn标题 */
@property (nonatomic,copy)NSString *btnTitle;

/** 右边btn颜色 */
@property (nonatomic,strong)UIColor *btnColor;

/** 点击右边btn执行的block */
@property (nonatomic, copy) void(^clickRowBtnAction)(id object);

/** 开始编辑时执行的block */
@property (nonatomic, copy) void(^beginEditBlock)(id object);


/** 键盘上自定义的View */
@property (nonatomic,strong) UIView *inputAccessoryView;

@end
