//
//  L2CListWebViewRow.h
//  L2Cplat
//
//  Created by zhukaiqi on 2017/8/8.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListWebViewRow : L2CListRow

/** URL */
@property (nonatomic,copy)NSString *url;

/** webView加载完毕执行的block */
@property (nonatomic,copy)void(^webViewDidFinishLoad)(UIWebView *webView);




@end
