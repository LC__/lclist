//
//  L2CListStarViewRow.h
//  L2Cplat
//
//  Created by feaonline on 2017/5/10.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListStarViewRow : L2CListRow

//** 能否滑动 */
@property (nonatomic,assign,getter=isScroll)BOOL scoll;

@end
