//
//  L2CListRow.h
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface L2CListRow : NSObject



/** 是否显示右边对勾 默认不显示 */
@property(nonatomic, assign) BOOL isShowCheck;
/** 图片 */
@property (nonatomic, strong)UIImage *icon;

/** 标题 */
@property (nonatomic, copy)NSString *title;

/** 子标题 */
@property (nonatomic, copy)NSString *subTitle;

/** 子标题2 */
@property (nonatomic,copy)NSString *subTitle2;

/** 跳转输入框的限定长度 */
@property (nonatomic,assign)NSInteger length;

/** 行高 */
@property (nonatomic,assign)CGFloat rowHeight;

/** indexPath */
@property (nonatomic,strong)NSIndexPath *indexPath;

/** 存储返回的值 */
@property (nonatomic,strong)NSMutableDictionary *saveDict;

/** subtitleTextColor */
@property (nonatomic,strong)UIColor *subtitleTextColor;

/** titleTextColor */
@property (nonatomic,strong)UIColor *titleTextColor;

@property (nonatomic,strong) UIFont *titleFont;

@property (nonatomic,strong) UIFont *subTitleFont;

/** 记录选人信息 */
@property (nonatomic,strong)NSMutableArray *personArray;

/**
 identifier
 */
@property (nonatomic, copy) NSString *identifier;

/** 是否显示本行分割线 默认显示  */
@property (nonatomic,assign,getter=isShowSeparator)BOOL showSeparator;


/** 是否成为第一响应者 */
@property (nonatomic,assign,getter=isBecomeFirstResponder)BOOL becomeFirstResponder;

/** 是否必填 */
@property (nonatomic,assign,getter=isMustTag)BOOL mustTag;

/** 点击这一行要做的事情 */
@property (nonatomic, copy) void(^clickRowAction)(id object);

+ (instancetype)itemWithIcon:(UIImage *)icon title:(NSString *)title mustTag:(BOOL)mustTag;

+ (instancetype)itemWithTitle:(NSString *)title mustTag:(BOOL)mustTag;
@end
