//
//  L2CListSwitchRow.h
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListSwitchRow : L2CListRow

/** 开关是否打开 */
@property (nonatomic,assign,getter=isOn)BOOL on;

/** 是否可以打开开关  默认可以打开*/
@property (nonatomic,assign,getter=isOpen)BOOL open;

@end
