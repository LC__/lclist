//
//  L2CListCustomRow.h
//  L2Cplat
//
//  Created by zhukaiqi on 2017/8/8.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"


@interface L2CListCustomRow : L2CListRow

/** 自定义视图的类 必须要有匹配的 xib */
@property (nonatomic,strong)Class clazz;

/** id */
@property (nonatomic,strong)id model;

/** 是否第一次加载 */
@property (nonatomic,assign,getter=isFirst)BOOL first;

/** 刷新自定义视图的需要执行的block */
@property (nonatomic,copy)void (^refreshCustomBlock)(id cutomView ,id item);

/** 保存当前自定义的view */
@property (strong,nonatomic) UIView *customView;

@end
