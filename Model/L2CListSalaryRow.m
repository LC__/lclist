//
//  L2CListSalaryRow.h
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/11/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListSalaryRow.h"

@implementation L2CListSalaryRow

- (void)setItemArr:(NSMutableArray *)itemArr{
    NSMutableArray *arrTemp = @[].mutableCopy;
    for (NSDictionary *info in itemArr) {
        NSMutableDictionary *infoM = [NSMutableDictionary dictionaryWithDictionary:info];
        [infoM setObject:@(0) forKey:@"height"];
        [arrTemp addObject:infoM];
    }
    _itemArr = arrTemp;
}

@end
