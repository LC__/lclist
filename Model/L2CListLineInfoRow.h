//
//  L2CListLineInfoRow.h
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/8/16.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListLineInfoRow : L2CListRow

/**
 点 的图片名
 PointImageName
 */
@property (nonatomic, copy) NSString *pointImgName;

/**
 线的颜色
 */
@property (nonatomic, strong) UIColor *lineColor;

/**
 信息数组
 @code
 // 内部模型示例
   @{
        @"title" : @"标题示例",
        @"info"  : @"信息示例",
    }
 @endcode
 */
@property (nonatomic, strong) NSMutableArray *lineInfoArr;

/** 属性字符标题 */
@property (nonatomic, strong) NSAttributedString *attTitle;

/**
 标题后附加的 标签
 
 默认值
 height ： 21
 
 @code
 @{
 @"tagTitle"        :   (NSString)@"标签标题",
 @"tagTextColor"    :   (UIColor) @"字以及边框颜色",
 @"tagBgColor"      :   (UIColor) @"标签背景颜色",
 @"width"           :   (NSString)@"宽度",
 }
 @endcode
 
 */
@property (nonatomic, strong) NSArray *tagInfoArr;

/**
 是否隐藏前面的线 默认 NO：不隐藏
 */
@property(nonatomic, assign) BOOL isHideLine;

#pragma mark - 右端按钮，默认为附件图片

/** 图片 */
@property (nonatomic, copy) NSString *btnImgName;

/** 标题 */
@property (nonatomic, copy) NSString *btnTitle;

/**
 是否显示右边Btn 默认 NO：不显示
 */
@property(nonatomic, assign) BOOL isShowRightBtn;

/**
 btn点击事件
 */
@property(nonatomic, copy) void(^clickBtnAction)(id sender);

/** item 标题的颜色 默认灰色*/
@property (nonatomic,strong) UIColor *itemTColor;


/** item 内容的颜色 默认黑色*/
@property (nonatomic,strong) UIColor *itemCColor;


@end
