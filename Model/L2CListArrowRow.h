//
//  L2CListArrowRow.h
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListArrowRow : L2CListRow

/** 非全天时间 */
@property (nonatomic,copy)NSString *noAllDayTime;

/** placeHolder */
@property (nonatomic,copy)NSString *placeHolder;

/** 是否显示占位字符 */
@property (nonatomic,assign,getter=isHiddenPlaceHolder)BOOL hiddenPlaceHolder;

/** 是否隐藏箭头 默认显示 */
@property(nonatomic, assign) BOOL isHideJiantou;




@end
