//
//  L2CListIndicatorRow.h
//  L2Cplat
//
//  Created by feaonline on 2017/8/8.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListIndicatorRow : L2CListRow

/** 字体大小 */
@property (assign,nonatomic) CGFloat textFont;

@end
