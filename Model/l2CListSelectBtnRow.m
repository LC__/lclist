//
//  l2CListSelectBtnRow.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "l2CListSelectBtnRow.h"

@implementation l2CListSelectBtnRow


- (void)setArray:(NSArray *)array{
    
        _array = array;

        CGFloat row = ceilf(array.count/4.0);
    
        self.rowHeight = 57 + row * (34 + 10);
    
        self.hiddenDate = YES;

}
@end
