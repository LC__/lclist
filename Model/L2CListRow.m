//
//  L2CListRow.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@implementation L2CListRow

- (UIFont *)titleFont{
    if (!_titleFont) {
        _titleFont = [UIFont systemFontOfSize:16];
    }
    return _titleFont;
}

- (UIFont *)subTitleFont{
    if (!_subTitleFont) {
        _subTitleFont = [UIFont systemFontOfSize:16];
    }
    return _subTitleFont;
}


- (instancetype)init {

    if (self = [super init]) {
        
        _showSeparator = YES;
    }
    
    return self;

}

+ (instancetype)itemWithIcon:(UIImage *)icon title:(NSString *)title mustTag:(BOOL)mustTag{
    
    L2CListRow *row = [[self alloc] init];
    
    row.icon = icon;
    
    row.title = title;
    
    row.mustTag = mustTag;
    
    row.subTitle = @"";
    
    row.subTitle2 = @"";
    
    row.saveDict = [NSMutableDictionary dictionary];
    
    row.becomeFirstResponder = NO;
    
    return row;
}

- (NSString *)identifier{
    
    return NSStringFromClass([self class]);
}

+ (instancetype)itemWithTitle:(NSString *)title mustTag:(BOOL)mustTag {
    
    return [self itemWithIcon:nil title:title mustTag:mustTag];
    
}
@end
