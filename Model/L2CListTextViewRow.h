//
//  L2CListTextViewRow.h
//  L2Cplat
//
//  Created by zhukaiqi on 2017/5/9.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListTextViewRow : L2CListRow

/** 占位字符串 */
@property (nonatomic,copy)NSString *placeHolder;

//** 能否编辑 */
@property (nonatomic,assign,getter=isNoEditor)BOOL noEditor;

/** 设置最大字数 默认不显示，设置后显示 */
@property (nonatomic,assign)NSInteger maxWords;


@end
