//
//  L2CListSwitchRow.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListSwitchRow.h"

@implementation L2CListSwitchRow


- (instancetype)init {

    if (self = [super init]) {
        
        self.open = YES;
    }
    
    return self;

}

@end
