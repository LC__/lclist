//
//  L2CListTwoTextFiledRow.h
//  L2Cplat
//
//  Created by zhukaiqi on 2017/5/10.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListTwoTextFiledRow : L2CListRow

/** 第一个占位字符 */
@property (nonatomic,copy)NSString *firstPlaceHolder;

/** 第二个占位字符 */
@property (nonatomic,copy)NSString *lastPlaceHolder;

/** 键盘类型 */
@property (nonatomic,assign)UIKeyboardType keyboardType;

/** 是否可以输入 */
@property (nonatomic,assign,getter=isInput) BOOL input;
@end
