//
//  L2CListProgressRow.h
//  L2Cplat
//
//  Created by feaonline on 2017/8/16.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

@interface L2CListProgressRow : L2CListRow

/** 审批流状态图片 */
@property (strong,nonatomic) UIImage *statusImage;

/** 审批人头像 */
@property (strong,nonatomic) NSString *approverImagePath;

/** 审批人 */
@property (strong,nonatomic) NSString *approverName;

/** 审批时间 */
@property (copy,nonatomic) NSString *approverTime;

/** 处理状态 */
@property (copy,nonatomic) NSString *approverStatus;

/** 备注 */
@property (copy,nonatomic) NSString *remark;

/** 附件个数 */
@property (assign,nonatomic) NSInteger fileNum;

@end
