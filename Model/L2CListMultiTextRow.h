//
//  L2CListMultiTextRow.h
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/6/3.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRow.h"

/**
 多行文字内容展示的行模型，没有标题，内部只含有一个Label
 @warning  如果没有行高那么就用文字所占的行高，否则显示不会超过设置的行高
 */
@interface L2CListMultiTextRow : L2CListRow

/** 字体大小 */
@property (assign,nonatomic) CGFloat textFont;
/** 字体颜色 */
@property (strong,nonatomic) UIColor *textColor;


@end
