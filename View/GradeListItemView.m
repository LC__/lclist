//
//  GradeListItemView.m
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/8/16.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "GradeListItemView.h"

@implementation GradeListItemView

- (void)awakeFromNib{
    [super awakeFromNib];
    self.autoresizingMask = UIViewAutoresizingNone;
    self.bgView.layer.cornerRadius = 2;
    self.bgView.layer.masksToBounds = YES;
}

@end
