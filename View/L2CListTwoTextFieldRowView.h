//
//  L2CListTwoTextFieldRowView.h
//  L2Cplat
//
//  Created by zhukaiqi on 2017/5/10.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "L2CListRowCell.h"
@interface L2CListTwoTextFieldRowView : UIView

/** 行模型 */
@property (nonatomic,strong)L2CListTwoTextFiledRow *row;

@end
