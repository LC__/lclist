//
//  L2CListLineInfoView.m
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/8/16.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListLineInfoView.h"
#import "NSString+StrSize.h"
#import "NSAttributedString+AttrStrSize.h"
#import "NotChangeColorLa.h"


@interface L2CListLineInfoView()
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleH;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoS;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagH;

@property (weak, nonatomic) IBOutlet UIView *tagView;

@property (weak, nonatomic) IBOutlet UIImageView *pointIv;
@property (weak, nonatomic) IBOutlet UILabel *line1;
@property (weak, nonatomic) IBOutlet UILabel *line;
@property (weak, nonatomic) IBOutlet UIView *infoListView;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;

/** titlwW */
@property(nonatomic, assign) CGFloat tW;

/**
 显示内容宽度
 */
@property(nonatomic, assign) CGFloat infoW;
@end

@implementation L2CListLineInfoView


- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    self.pointIv.layer.cornerRadius = self.pointIv.width / 2;
//    self.pointIv.layer.masksToBounds = YES;
    
}
- (void)configLine{
    
    self.pointIv.hidden = self.row.isHideLine;
    self.line.hidden = self.row.isHideLine;
    self.line1.hidden = self.row.isHideLine;
    
    self.infoS.constant = self.row.isHideLine ? 15 : 36.0;
    
    self.infoW = kWidth - self.infoS.constant - 15;
}

- (void)setRow:(L2CListLineInfoRow *)row{
    _row = row;
    
    [self configLine];
    [self removeAllTags];
    
    if (row.tagInfoArr.count) {
        [self setTagList];
    }
    
    self.titleLa.text = @"";
    self.titleLa.attributedText = nil;
    if (row.attTitle) {
        self.titleLa.attributedText = row.attTitle;
    }else{
        self.titleLa.text = row.title;
    }
    
    CGFloat width = [self.titleLa.text sizeWithFont:self.titleLa.font and:CGSizeMake(MAXFLOAT, self.titleLa.height)].width + 5;
    width = ceil(MaxEqual(width, self.infoW - self.tagW.constant));
    self.titleW.constant = width;
    if (!row.title.length && !row.attTitle.length) {
        self.titleH.constant = 0.0;
    }else{
        self.titleH.constant = 21.0;
    }
    
    if (row.pointImgName) {
        self.pointIv.backgroundColor = [UIColor clearColor];
        self.pointIv.image = [UIImage imageNamed:row.pointImgName];
    }else{
        self.pointIv.image =  nil;
        self.pointIv.backgroundColor = row.lineColor;
    }
    
    self.line.backgroundColor = row.lineColor;
    
//    if (!row.rowHeight) {
        [self setInfoList];
//    }
    
    [self configBtn];
    
    [self layoutIfNeeded];
}

- (void)setTagList{
    CGFloat curX = 0.0;
    for (NSMutableDictionary *tagInfo in self.row.tagInfoArr) {
        
        UILabel *tagLa = [self creTagLabel];
        tagLa.text = tagInfo[@"tagTitle"];
        
        UIColor *textColor = (UIColor *)tagInfo[@"tagTextColor"];
        UIColor *bgColor = (UIColor *)tagInfo[@"tagBgColor"];
        
        tagLa.layer.borderColor = textColor.CGColor;
        tagLa.textColor = textColor;
        tagLa.layer.backgroundColor = bgColor.CGColor;
        tagLa.backgroundColor = bgColor;
       
        CGFloat width = [tagInfo[@"width"] floatValue];
        if (!width) {
            
            width = ceil([tagLa.text sizeWithFont:tagLa.font and:CGSizeMake(MAXFLOAT, tagLa.height)].width + 7);
            tagInfo[@"width"] = @(width);
            
        }
        
        width = MaxEqual(width, 55);
        tagLa.width = width;
        tagLa.x = curX + 5;
        
        curX = CGRectGetMaxX(tagLa.frame);
        
        [self.tagView addSubview:tagLa];
    }
    self.tagW.constant = curX;
}

- (void)removeAllTags{
    for (UIView *tagLa in self.tagView.subviews) {
        [tagLa removeFromSuperview];
    }
    self.tagW.constant = 0.0;
}

- (void)configInfoW{
    CGFloat width = 65;
    for (NSMutableDictionary *info in self.row.lineInfoArr) {
        UIButton *titleLa = [self creTitleLabel];
        CGFloat w = ceil([info[@"title"] sizeWithFont:titleLa.titleLabel.font and:CGSizeMake(MAXFLOAT,ceil(titleLa.height))].width + 5);
        if (w > width) {
            width = w;
        }
        if (width >= 100) {
            break;
        }
    }
    width = MaxEqual(width, 100);
    self.tW = width;
}

- (void)setInfoList{
    
    for (UIView *view in self.infoListView.subviews) {
        [view removeFromSuperview];
    }
    
    CGFloat curX = 0.0;
    CGFloat curY = -5;
    
    [self configInfoW];
    
    for (NSMutableDictionary *info in self.row.lineInfoArr) {
        
        UIButton *titleLa = [self creTitleLabel];
        [titleLa setTitle:info[@"title"] forState:(UIControlStateNormal)];
        
        titleLa.x = curX;
        titleLa.y = curY + 8;
        
        NSString *infoStr = info[@"info"];
        if ([infoStr hasPrefix:@"STAR:"]) {
            
        }else{
            UIButton *infoLa = [self creInfoLabel];
            [infoLa setTitle:info[@"info"] forState:(UIControlStateNormal)];
            infoLa.x = CGRectGetMaxX(titleLa.frame) + 5;
            infoLa.y = curY + 8;
            CGFloat infoH = [info[@"height"] floatValue];
            
            if (!infoH) {
                infoH = ceil([infoLa.titleLabel.text sizeWithFont:infoLa.titleLabel.font and:CGSizeMake(ceil(infoLa.width), MAXFLOAT)].height + 2);
                info[@"height"] = @(infoH);
            }
            
            infoH = MinEqual(infoH, 20);
            infoLa.height = infoH;
            
            curY = CGRectGetMaxY(infoLa.frame);
            [self.infoListView addSubview:infoLa];
        }
        
        [self.infoListView addSubview:titleLa];
        
    }
    
    _row.rowHeight = curY + 35 + self.titleH.constant;
}

- (UILabel *)creTagLabel{
    
    UILabel *tagLa = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, 50, 17)];
    tagLa.font = [UIFont systemFontOfSize:10];
    tagLa.textAlignment = NSTextAlignmentCenter;
    tagLa.layer.cornerRadius = 3;
    tagLa.layer.masksToBounds = YES;
    tagLa.layer.borderWidth = 1;
    
    return tagLa;
}

- (UIButton *)creTitleLabel{
    
    UIButton *title = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.tW, 20)];
    title.userInteractionEnabled = NO;
    title.titleLabel.font = [UIFont systemFontOfSize:14];
//    title.titleLabel.numberOfLines = 0;
    
    [title setTitleColor:self.row.itemTColor forState:(UIControlStateNormal)];
    title.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    title.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

    return title;
}

- (UIButton *)creInfoLabel{
    
    UIButton *info = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, ceil(self.infoW - self.tW - 5), 20)];
    info.userInteractionEnabled = NO;
    info.titleLabel.font = [UIFont systemFontOfSize:14];
    info.titleLabel.numberOfLines = 0;
   
    [info setTitleColor:self.row.itemCColor forState:(UIControlStateNormal)];
    info.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    info.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    return info;
}

- (void)configBtn{
    self.rightBtn.hidden = !self.row.isShowRightBtn;
    [self.rightBtn setTitle:self.row.btnTitle forState:(UIControlStateNormal)];
    [self.rightBtn setImage:[UIImage imageNamed:self.row.btnImgName] forState:(UIControlStateNormal)];
}

- (IBAction)rightBtnClick:(id)sender {
    if (self.row.clickBtnAction) {
        self.row.clickBtnAction(sender);
    }
}



@end
