//
//  L2CListShowMoreHeader.m
//  L2Cplat
//
//  Created by L2Cplat on 2018/9/12.
//  Copyright © 2018年 feaonline. All rights reserved.
//

#import "L2CListShowMoreHeader.h"

@implementation L2CListShowMoreHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)click:(UIButton *)sender {
    self.isOpen = !self.isOpen;
    sender.selected = self.isOpen;
    if (self.headerClick) {
        self.headerClick(self.isOpen);
    }
}

@end
