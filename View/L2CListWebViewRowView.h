//
//  L2CListWebViewRowView.h
//  L2Cplat
//
//  Created by zhukaiqi on 2017/8/8.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "L2CListRowCell.h"

@interface L2CListWebViewRowView : UIView

/** webViewRow */
@property (nonatomic,strong)L2CListWebViewRow *row;

@end
