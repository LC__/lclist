//
//  L2CListTwoTextFieldRowView.m
//  L2Cplat
//
//  Created by zhukaiqi on 2017/5/10.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListTwoTextFieldRowView.h"

@interface L2CListTwoTextFieldRowView ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *mustTagLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UITextField *firstTextField;

@property (weak, nonatomic) IBOutlet UITextField *lastTextField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelWidth;
@property (weak, nonatomic) IBOutlet UIButton *firstButton;
@property (weak, nonatomic) IBOutlet UIButton *lastButton;

@end

@implementation L2CListTwoTextFieldRowView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    self.firstTextField.delegate = self;
    
    self.lastTextField.delegate = self;
    
    self.firstTextField.tag = 1001;
    self.lastTextField.tag = 1002;
    
    
}
- (void)setRow:(L2CListTwoTextFiledRow *)row {
    
    _row = row;
    
    self.mustTagLabel.hidden = !row.isMustTag;
    
    self.titleLabel.text = row.title;
    
    self.firstTextField.text = row.subTitle;
    
    self.lastTextField.text = row.subTitle2;
    
    self.titleLabelWidth.constant = [L2CListRowCell fetchTitleLableWidthWithStr:row.title];
    
    self.firstButton.userInteractionEnabled = !row.isInput;
    self.lastButton.userInteractionEnabled = !row.isInput;
    
    if (row.firstPlaceHolder) {
        
        self.firstTextField.placeholder = row.firstPlaceHolder;
        self.lastTextField.placeholder = row.lastPlaceHolder;
        
    }
    else{
        
        if (!row.isMustTag) {
            
            self.firstTextField.placeholder = L2CLocalized(@"public.Optional field", @"Public",@"选填");
            self.lastTextField.placeholder = L2CLocalized(@"public.Optional field", @"Public",@"选填");
        }
        else {
            self.firstTextField.placeholder = L2CLocalized(@"public.Required field", @"Public",@"必填");
            self.lastTextField.placeholder = L2CLocalized(@"public.Required field", @"Public",@"必填");
            
        }
    }
    
    if (row.keyboardType) {
        
        self.firstTextField.keyboardType = row.keyboardType;
        self.lastTextField.keyboardType = row.keyboardType;
    }
    
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    return [textField resignFirstResponder];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    [self performSelector:@selector(setText:) withObject:textField afterDelay:0.03];
    
    return YES;
    
}


- (void)setText:(UITextField *)textField{
    
    if (self.row.isInput) {
        
        if (self.row.clickRowAction) {
            
            self.row.clickRowAction(textField);
        }
    }
   
    
    if (textField.tag ==1001) {
        
         self.row.subTitle = textField.text;
    }
    else {
    
        self.row.subTitle2 = textField.text;
    }
   
    
    
}

- (IBAction)clickFirstButtonAction:(UIButton *)sender {
    
    
    if (self.row.isInput==NO) {
        
        [KeyWindow endEditing:YES];
    }
    
    if (self.row.clickRowAction) {
        
        self.row.clickRowAction(@"first");

    }
    
    L2CLog(@"%s",__func__);
}

- (IBAction)clickLastButtonAction:(UIButton *)sender {
    
    
    if (self.row.isInput==NO) {
        
        [KeyWindow endEditing:YES];
    }
    
    if (self.row.clickRowAction) {
        
        self.row.clickRowAction(@"last");
        
    }
    
    L2CLog(@"%s",__func__);
}



@end
