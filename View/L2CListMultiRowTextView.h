//
//  L2CListMultiRowTextView.h
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/6/3.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "L2CListRowCell.h"

/**
 多行文字内容展示的行视图，没有标题，内部只含有一个Label
 */
@interface L2CListMultiRowTextView : UIView

/**
 行模型
 */
@property (nonatomic, strong) L2CListMultiTextRow *row;
@end
