//
//  L2CListSwitchRowView.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListSwitchRowView.h"

@interface L2CListSwitchRowView ()
@property (weak, nonatomic) IBOutlet UILabel *mustTagLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UISwitch *switchView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelWidth;


@end

@implementation L2CListSwitchRowView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    
}

- (void)setRow:(L2CListSwitchRow *)row {
    
    _row = row;
    
    self.mustTagLabel.hidden = !row.isMustTag;
    
    self.titleLabel.text = row.title;
    
    self.switchView.on = row.isOn;
    
    self.switchView.userInteractionEnabled = row.isOpen;
    
    self.titleLabelWidth.constant = [L2CListRowCell fetchTitleLableWidthWithStr:row.title];
    
}

- (IBAction)clickSwitchAction:(UISwitch *)sender {
    
    [KeyWindow endEditing:YES];
    
    self.row.on = sender.isOn;
    
    if (sender.isOn) {
        
        self.row.subTitle = @"1";
    }
    
    if (self.row.clickRowAction) {
        
        self.row.clickRowAction(@(sender.isOn));
    }
}


@end
