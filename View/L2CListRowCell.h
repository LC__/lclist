//
//  L2CListRowCell.h
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "L2CListDefultRow.h"
#import "L2CListRow.h"
#import "l2CListSelectBtnRow.h"
#import "L2CListArrowRow.h"
#import "L2CListSwitchRow.h"
#import "L2CListTextFieldRow.h"
#import "L2CListSelectBtnAndArrowRow.h"
#import "L2CListIconRow.h"
#import "L2CListSection.h"
#import "L2CListTextViewRow.h"
#import "L2CListStarViewRow.h"
#import "L2CListContentViewRow.h"
#import "L2CListTwoTextFiledRow.h"
#import "L2CListMultiTextRow.h"
#import "L2CListButtonArrayRow.h"
#import "L2CListIndicatorRow.h"
#import "L2CListCustomRow.h"
#import "L2CListWebViewRow.h"
#import "L2CListLineInfoRow.h"
#import "L2CListGradeInfoRow.h"
#import "L2CListProgressRow.h"
#import "L2CListTagLabelsRow.h"
#import "L2CListSalaryRow.h"
#import "L2CIndContentRow.h"
#import "L2CListEchartsListRow.h"

#import "NSString+StrSize.h"

@interface L2CListRowCell : UITableViewCell

//+ (instancetype)cellWithTableView:(UITableView *)tableView;

+ (instancetype)cellWithTableView:(UITableView *)tableView andIdentifier:(NSString *)identifier;

/** 行模型 */
@property (nonatomic, strong) L2CListRow *row;

//计算标题的宽度
+ (CGFloat )fetchTitleLableWidthWithStr:(NSString *)str;
@end
