//
//  L2CListIconViewRowView.h
//  L2Cplat
//
//  Created by zhukaiqi on 2017/4/19.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "L2CListRowCell.h"
@interface L2CListIconViewRowView : UIView


/** row */
@property (nonatomic,strong)L2CListIconRow *row;
@end
