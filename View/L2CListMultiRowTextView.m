//
//  L2CListMultiRowTextView.m
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/6/3.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListMultiRowTextView.h"
#import "NSString+StrSize.h"


@interface L2CListMultiRowTextView ()

@property (weak, nonatomic) IBOutlet UILabel *contentLa;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentW;

@end

@implementation L2CListMultiRowTextView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    self.contentW.constant = ceil(kWidth - 30);
    
}

- (void)setRow:(L2CListMultiTextRow *)row{
    
    _row = row;
    
    self.contentLa.text = row.subTitle;
    
    if(row.textFont){
    
        self.contentLa.font = [UIFont systemFontOfSize:row.textFont];
    }
    
    if(row.textColor){
    
        self.contentLa.textColor = row.textColor;
    }
    
    CGSize size = CGSizeMake(ceil(kWidth - 30), MAXFLOAT);
    
    CGSize stringSize = [self.contentLa.text boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:self.contentLa.font} context:nil].size;
    
    CGFloat tureHeight = ceil(stringSize.height+20);
    
    
    self.row.rowHeight = tureHeight;
    
    
    self.row.rowHeight = MinEqual(self.row.rowHeight, 40);
   
    
    
}

@end
