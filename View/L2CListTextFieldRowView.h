//
//  L2CListTextFieldRowView.h
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "L2CListRowCell.h"
@interface L2CListTextFieldRowView : UIView

/** 行模型 */
@property (nonatomic,strong)L2CListTextFieldRow *row;
@end
