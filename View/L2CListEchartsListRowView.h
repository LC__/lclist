//
//  L2CListEchartsListRowView.h
//  L2Cplat
//
//  Created by L2Cplat on 2018/3/2.
//  Copyright © 2018年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "L2CListRowCell.h"

@interface L2CListEchartsListRowView : UIView

/** EchartsListRow */
@property (nonatomic,strong)L2CListEchartsListRow *row;



@end
