//
//  L2CListContentViewRowView.m
//  L2Cplat
//
//  Created by feaonline on 2017/5/10.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListContentViewRowView.h"
#import "NSString+StrSize.h"


@interface L2CListContentViewRowView ()

@property (weak, nonatomic) IBOutlet UILabel *mustTagLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conB;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conT;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conH;

@end

@implementation L2CListContentViewRowView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    

    
}

- (void)setRow:(L2CListContentViewRow *)row {
    
    
    _row = row;
    
    self.mustTagLabel.hidden = !row.isMustTag;
    
    self.titleLabel.text = row.title;
    self.titleLabel.font = row.ctitleFont;
  
    self.conH.constant = row.contentH;
    self.titleH.constant = row.titleH;
    self.conT.constant = row.conT;
    self.conB.constant = row.conB;
    
    self.contentLabel.text = row.subTitle;
    self.contentLabel.numberOfLines = row.linesNum;

    
    self.contentLabel.textColor = row.subtitleTextColor?row.subtitleTextColor:Color(102, 102, 102);
    
}
@end
