//
//  L2CListEchartsListRowView.m
//  L2Cplat
//
//  Created by L2Cplat on 2018/3/2.
//  Copyright © 2018年 feaonline. All rights reserved.
//

#import "L2CListEchartsListRowView.h"

@interface L2CListEchartsListRowView ()<WKEchartsViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (strong, nonatomic) WKEchartsView *chartsView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;
@property (weak, nonatomic) IBOutlet UILabel *ploView;



@end



@implementation L2CListEchartsListRowView

- (WKEchartsView *)chartsView {
    
    if (!_chartsView) {
        
        _chartsView = [[WKEchartsView alloc] initWithFrame:CGRectMake(0, 100, self.bgView.width, self.bgView.height)];
        
    }
    
    return _chartsView;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    [self.bgView addSubview:self.chartsView];
    
    self.chartsView.eDelegate = self;
   
    [self startLoading];
    
    
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    self.bgView.frame = CGRectMake(0, 10, frame.size.width, frame.size.height - 20);
    _chartsView.frame = self.bgView.bounds;
    _ploView.frame = self.bgView.bounds;
    _chartsView.width = self.bgView.width;
    _chartsView.height = self.bgView.height;
    L2CWeakSelf(self);
    
    self.loadingView.center = self.center;
    
    if (self.row.isRefreshFrame) {
        _chartsView.y = 10;

        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [weakself.chartsView resetDivFrame];
                [weakself.chartsView refreshEcharts];
                
            });
        });
    }
}

- (void)setRow:(L2CListEchartsListRow *)row {
    _row = row;
    [self startLoading];
    
    if (self.row.chartsListRowDidFinishLoad) {
        self.row.chartsListRowDidFinishLoad(self.chartsView);
    }
    
    if ([self isEmpty]) {
        [self showPloView];
    }else{
        [self hidePloView];
       
    }
    
//    空 option 也必须要设置
    L2CWeakSelf(self);

    dispatch_async(dispatch_get_global_queue(0, 0), ^{
       
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [weakself.chartsView setOption:weakself.row.chartOption];
            [weakself.chartsView loadEcharts];
            
        });
    });
    
}

- (void)showPloView{
    
    [self stopLoading];
    [self bringSubviewToFront:self.ploView];
    self.ploView.hidden = NO;
   
}

- (void)hidePloView{
    self.ploView.hidden = YES;
}

- (void)startLoading{
    
    [self bringSubviewToFront:self.loadingView];
    self.loadingView.hidden = NO;
    [self.loadingView startAnimating];
}

- (void)stopLoading{
    [self.loadingView stopAnimating];
    self.loadingView.hidden = YES;
}

- (void)echartsViewDidFinishLoad:(WKEchartsView *)echartsView {
    
    [self stopLoading];
    
    if (self.row.chartsListRowDidFinishLoad) {
        self.row.chartsListRowDidFinishLoad(self.chartsView);
    }
    
}

- (BOOL)echartsView:(PYEchartsView *)echartsView didReceivedLinkURL:(NSURL *)url {
    
    
    return NO;
    
}

- (BOOL)isEmpty{
  
    
    if (!self.row.chartOption.series.count) {
        return YES;
    }
    
    for (PYSeries *series in self.row.chartOption.series) {
        
        id data = series.data;
        if ([data isKindOfClass:[NSArray class]]) {
            NSArray *dataA = (NSArray *)data;
            if (dataA.count) {
                return NO;
            }
        }
        if (data && (![data isKindOfClass:[NSArray class]])) {
            return NO;
        }
    }
    
    return YES;
}



@end
