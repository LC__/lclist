//
//  L2CListRowVerticalButton.m
//  L2Cplat
//
//  Created by zhukaiqi on 2017/8/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRowVerticalButton.h"
#import "NSString+StrSize.h"
@implementation L2CListRowVerticalButton

- (instancetype)init{
    
    if (self = [super init]) {
        
        [self setup];
    }
    
    return self;
    
}


- (void)setup
{
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.titleLabel.font = [UIFont systemFontOfSize:kWidth==320 ? 10:12];
    
    self.titleLabel.numberOfLines = 0;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.centerX = self.width/2.0;
    
    self.imageView.bottom = self.height/2.0+3;
    
    self.titleLabel.y = self.imageView.bottom+5;
    
    self.titleLabel.width = self.width-12;
    
    self.titleLabel.centerX = self.width/2.0;
    
    self.titleLabel.height = [self.titleLabel.text sizeWithFont:self.titleLabel.font and:CGSizeMake(self.titleLabel.width, MAXFLOAT)].height;
    
}


@end
