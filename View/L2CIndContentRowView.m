//
//  L2CIndContentRowView.m
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/11/8.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CIndContentRowView.h"
@interface L2CIndContentRowView ()
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
@property (weak, nonatomic) IBOutlet UILabel *contentLa;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *sep1;
@property (weak, nonatomic) IBOutlet UILabel *line;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vTrailing;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property(nonatomic, assign) CGFloat infoW;

@end
@implementation L2CIndContentRowView

- (void)awakeFromNib{
    [super awakeFromNib];

    self.autoresizingMask = UIViewAutoresizingNone;
    self.bgView.layer.borderWidth = 0.5;
    self.bgView.layer.cornerRadius = 4;
}

- (void)setRow:(L2CIndContentRow *)row{
    _row = row;
    self.titleLa.text = row.title;
    [self configLine];
    [self setInfoList];
    
    [self setBtn];
    [self layoutIfNeeded];
    [self drawDash];
    [self layoutIfNeeded];
}

- (void)setBtn{
    if (!self.row.btnTitle.length) {
        self.rightBtn.hidden = YES;
        return;
    }
    self.rightBtn.hidden = NO;
    
    [self.rightBtn setTitle:self.row.btnTitle forState:(UIControlStateNormal)];
    [self.rightBtn setImage:[UIImage imageNamed:self.row.btnImg] forState:(UIControlStateNormal)];
    
}
- (IBAction)btnClick:(id)sender {
    L2CLog(@"Click");
    if (self.row.btnBlock) {
        self.row.btnBlock(self.row);
    }
}

- (void)drawDash{
    [self.sep1 addLineWithType:BMLineTypeCustomDash color:[UIColor lightGrayColor] position:BMLinePostionCustomTop];
}

- (void)configLine{
    self.line.hidden = !self.row.isShowLine;
  
    if (self.row.isShowLine) {
        self.vLeading.constant = 30.0;
        self.vTrailing.constant = 20.0;
        
        self.bgView.layer.borderColor = [UIColor lightGrayColor].CGColor;
       
        self.vBottom.constant = 15.0;
        self.bgView.backgroundColor = BackGroundColor;
    }else{
        self.vLeading.constant = 0.0;
        self.vTrailing.constant = 0.0;
        
        self.bgView.layer.borderColor = [UIColor clearColor].CGColor;
        self.vBottom.constant = 0.0;
        self.bgView.backgroundColor = [UIColor whiteColor];
    }
    self.infoW = kWidth - 30 - self.vLeading.constant - self.vTrailing.constant;
    
}

- (void)setInfoList{
    
    self.contentLa.text = self.row.subTitle;
    
    CGFloat infoH = ceil([self.contentLa.text sizeWithFont:self.contentLa.font and:CGSizeMake(ceil(self.infoW), MAXFLOAT)].height + 5);
    
    infoH = MinEqual(infoH, 25);
    
    _row.rowHeight = infoH + 35 + 25
    + self.vBottom.constant;
}

@end
