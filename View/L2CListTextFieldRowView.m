//
//  L2CListTextFieldRowView.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListTextFieldRowView.h"

@interface L2CListTextFieldRowView ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *mustTagLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UITextField *subtitleTextField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelWidth;

@property (weak, nonatomic) IBOutlet UIButton *btn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *taling;
@property (weak, nonatomic) IBOutlet UIImageView *spratorLine;

@end
@implementation L2CListTextFieldRowView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    self.subtitleTextField.delegate = self;
    
}

- (void)setRow:(L2CListTextFieldRow *)row {
    
    _row = row;
    
    self.mustTagLabel.hidden = !row.isMustTag;
    
    self.titleLabel.text = row.title;
    
    self.subtitleTextField.text = row.subTitle;
    
    self.subtitleTextField.keyboardType =row.keyboardType?row.keyboardType: UIKeyboardTypeDefault;
    
    self.subtitleTextField.textColor = row.subtitleTextColor?row.subtitleTextColor:Color(102,102,102);
    
    self.titleLabelWidth.constant = [L2CListRowCell fetchTitleLableWidthWithStr:row.title];
    
    self.subtitleTextField.inputAccessoryView = row.inputAccessoryView;
    
    if (row.isBecomeFirstResponder) {
        
        [self.subtitleTextField becomeFirstResponder];
        
    }
    
    if (row.placeHolder) {
        
        self.subtitleTextField.placeholder = row.placeHolder;

    }
    else{
    
        if (!row.isMustTag) {
            
            self.subtitleTextField.placeholder = L2CLocalized(@"public.Optional field", @"Public",@"选填");
        }
        else {
            self.subtitleTextField.placeholder = L2CLocalized(@"public.Required field", @"Public",@"必填");
            
        }
    }
    
    
    self.subtitleTextField.userInteractionEnabled = row.isInput;
    
    self.btn.hidden = !row.isShowBtn;
    
    self.taling.constant = 27;
    
    self.spratorLine.hidden = YES;
    
    if (row.btnTitle) {
        
        [self.btn setTitle:row.btnTitle forState:UIControlStateNormal];
        
        [self.btn setImage:nil forState:UIControlStateNormal];
        
    
    }
    else if (row.btnImageName) {
        
        [self.btn setImage:[UIImage imageNamed:row.btnImageName] forState:UIControlStateNormal];
        
        [self.btn setTitle:nil forState:UIControlStateNormal];
        
        self.taling.constant = 37;
        
        self.spratorLine.hidden = NO;
    }
   
    if (row.btnColor) {
        
        [self.btn setTitleColor:row.btnColor forState:UIControlStateNormal];
    }
    
    
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if (self.row.beginEditBlock) {
            
            self.row.beginEditBlock(textField.text);
        }
    });
    
    return YES;

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    return [textField resignFirstResponder];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    [self performSelector:@selector(setText:) withObject:textField afterDelay:0.03];
    
    return YES;
    
}


- (void)setText:(UITextField *)textField{
    
    
     self.row.subTitle = textField.text;
    
    if (self.row.clickRowAction) {
        
        self.row.clickRowAction(textField);
    }
  
}



- (IBAction)clickBtnAction:(UIButton *)sender {
    
    
    if (self.row.clickRowBtnAction) {
        
        self.row.clickRowBtnAction(self.row);
    }
    
}
@end
