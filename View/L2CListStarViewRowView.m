//
//  L2CListStarViewRowView.m
//  L2Cplat
//
//  Created by feaonline on 2017/5/10.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListStarViewRowView.h"
#import "RatingBar.h"
@interface L2CListStarViewRowView ()<RatingBarDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *mustTagLabel;
@property (weak, nonatomic) IBOutlet RatingBar *starView;

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@end

@implementation L2CListStarViewRowView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;

    [self.starView setImageDeselected:@"opdca_icon_daxing0" halfSelected:@"opdca_icon_daxinghalf" fullSelected:@"opdca_icon_daxing1" andDelegate:self];
    
}

- (void)setRow:(L2CListStarViewRow *)row {

    _row = row;
    
    self.titleLabel.text = row.title;
    
    self.scoreLabel.text = row.subTitle;
    
    self.mustTagLabel.hidden = !row.mustTag;

    self.contentLabel.text = row.subTitle2;
    
    self.starView.isIndicator = row.isScroll;
    
    [self.starView displayRating:[row.subTitle floatValue]];
    
    self.row.rowHeight = [self rowHeight];

}

-(void)ratingBar:(RatingBar *)ratingBar ratingChanged:(float)newRating{
    
    
    NSString *grade = L2CLocalized(@"public.invalid", @"Public",@"无效");
    
    if (newRating>4.5) {
        
        grade = L2CLocalized(@"public.youyi", @"Public",@"优异");
    }
    else if (newRating>4) {
    
        grade = L2CLocalized(@"public.youxiu", @"Public",@"优秀");
    }
    else if (newRating>3.5) {
        
        grade = L2CLocalized(@"public.youliang", @"Public",@"优良");
    }
    else if (newRating>3) {
        
        grade = L2CLocalized(@"public.liang", @"Public",@"良");
    }
    else if (newRating>2.5) {
        
        grade = L2CLocalized(@"public.hege", @"Public",@"合格");
    }
    else if (newRating>2) {
        
        grade = L2CLocalized(@"public.buhege", @"Public",@"不合格");
    }
    else if (newRating>1.5) {
        
        grade = L2CLocalized(@"public.cha", @"Public",@"差");
    }
    else if (newRating>1) {
        
        grade = L2CLocalized(@"public.hencha", @"Public",@"很差");
    }
    else if (newRating>0.5) {
        
        grade = L2CLocalized(@"public.feichangcha", @"Public",@"非常差");
    }
    else {
        grade = L2CLocalized(@"public.invalid", @"Public",@"无效");
    }
    self.scoreLabel.text = [NSString stringWithFormat:@"%.1f%@(%@)",newRating,L2CLocalized(@"public.points", @"Public",@"分"),grade];
        
    if([self.scoreLabel.text isEqualToString:@"0.0分"])
    {
        NSString *str = [NSString stringWithFormat:@"0%@(%@)",L2CLocalized(@"public.points", @"Public",@"分"),L2CLocalized(@"public.invalid", @"Public",@"无效")];
        self.scoreLabel.text = str;
    }
    self.row.subTitle = [NSString stringWithFormat:@"%.1f",newRating];
    
  
    
    
}


-(CGFloat)rowHeight{
    
    CGSize size = CGSizeMake(ceil(kWidth - 42), MAXFLOAT);
    
    CGSize stringSize = [self.contentLabel.text boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:self.contentLabel.font} context:nil].size;
    
    CGFloat height = ceil(stringSize.height+60);
    
    if(self.contentLabel.text.length > 0)
    {
        self.contentLabel.hidden = NO;

    }else{
      
        self.contentLabel.hidden = YES;
        
        height = 57;
    }
    
    return height;
}

@end
