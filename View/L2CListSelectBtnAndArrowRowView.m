//
//  L2CListSelectBtnAndArrowRowView.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListSelectBtnAndArrowRowView.h"

@interface L2CListSelectBtnAndArrowRowView ()

@property (weak, nonatomic) IBOutlet UILabel *mustTagLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UITextField *subtitleTextField;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelWidth;

@end

@implementation L2CListSelectBtnAndArrowRowView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    
}

- (void)setRow:(L2CListSelectBtnAndArrowRow *)row {
    
    _row = row;
    
    self.mustTagLabel.hidden = !row.isMustTag;

    
    if (row.isExpand) {
        
        self.arrowImage.image = [UIImage imageNamed:@"up"];
    }
    else {
        
        self.arrowImage.image =[UIImage imageNamed:@"down"];
    }
    
    self.subtitleTextField.text = row.subTitle2;
    
    self.titleLabelWidth.constant = [L2CListRowCell fetchTitleLableWidthWithStr:row.title];
    
    self.titleLabel.text = row.title;
    
    
    if (!row.isMustTag) {
        
        self.subtitleTextField.placeholder = L2CLocalized(@"public.Optional field", @"Public",@"选填");
    }
    else {
        self.subtitleTextField.placeholder = L2CLocalized(@"public.Required field", @"Public",@"必填");
        
    }
    
    if (row.isExpand) {
        
       [self setBtnLayoutSubViewsWith:row.array tag:row.subTitle];
    }
    else {
      
        [self setBtnLayoutSubViewsWith:nil tag:nil];
    
    }
    
   
    
    
}
//添加button
- (void)setBtnLayoutSubViewsWith:(NSArray *)array tag:(NSString *)tags {
    
    //添加前 先移除所有视图
    for (UIView *view in self.bgView.subviews) {
        
        [view removeFromSuperview];
    }
  
    //bt的个数
    NSInteger btCount = array.count;
    
    //bt的长宽高
    CGFloat btX = 15;
    CGFloat btY = 0;
    CGFloat btW = (self.frame.size.width - 3*5 - 15-10 )/4.0;
    CGFloat btH =  34;
    
    if ( btCount) {
        
        NSInteger ro = (btCount-1)/4;
        
        for (NSInteger i = 0; i<btCount; i++) {
            
            //bt的行
            NSInteger row = i/4;
            
            //bt的列
            NSInteger col = i%4;
            
            NSString *title = array[i][@"value"];
            NSString *tagId = array[i][@"id"];
            UIButton *bt = [UIButton buttonWithType:UIButtonTypeCustom];
            
            bt.titleLabel.numberOfLines = 0;
            bt.titleLabel.textAlignment = NSTextAlignmentCenter;
//            if (title.length > 5) {
//                
//                NSString *str1 = [title substringToIndex:3];
//                NSString *str2 = [title substringFromIndex:3];
//                title = [NSString stringWithFormat:@"%@\n%@",str1,str2];
//            }
            
            bt.frame = CGRectMake(btX+(5+btW)*col, btY+(5+btH)*row, btW, btH);
            [bt setTitle:title forState:UIControlStateNormal];
            
            //若果选中时
            if ([tags isEqualToString:tagId] &&![tags isEqualToString:@""]) {
                
                [bt setTitleColor:[UIColor colorWithRed:0.1059 green:0.3725 blue:0.8431 alpha:1.0] forState:UIControlStateNormal];
                [bt setBackgroundImage:[UIImage imageNamed:@"SX_bg_xuanxiang_"] forState:UIControlStateNormal];
                
            }
            else {
                
                [bt setTitle:title forState:UIControlStateNormal];
                [bt setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                bt.backgroundColor =   Color(238, 238, 238);
            }
            bt.titleLabel.font = [UIFont systemFontOfSize:12];
            [bt addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
            bt.tagId =tagId;
            bt.layer.cornerRadius = 3;
            bt.layer.masksToBounds = YES;
            [self.bgView  addSubview:bt];
            
        }
        self.bgViewHeight.constant = (ro+1)*(btH+5);
        
    }
    
    
}

- (void)clickAction:(UIButton *)sender {
    
    [KeyWindow endEditing:YES];
    
    NSString *selectTitle = [sender.titleLabel.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    NSString *selectId = sender.tagId;
    
    if ([SafeString(selectId ) isEqualToString:SafeString(self.row.subTitle)]) {
        
        return ;
    }

    self.row.title = selectTitle;
    
    self.row.subTitle = selectId;
   
    self.titleLabel.text = selectTitle;
    
    self.row.saveDict = [@{@"id":selectId,@"value":selectTitle} mutableCopy];
    
    [self setBtnLayoutSubViewsWith:self.row.array tag:selectId];
    
    self.titleLabelWidth.constant = [L2CListRowCell fetchTitleLableWidthWithStr:selectTitle];
    

    if (self.row.clickSubBtnAction) {
        
        self.row.clickSubBtnAction(self.row);
    }
    
  
   
    
}

- (IBAction)clickTitleBtnAction:(UIButton *)sender {
    
     [KeyWindow endEditing:YES];
    
    self.row.expand = !self.row.isExpand;

    
    if (!self.row.isExpand) {
        
        self.bgViewHeight.constant = 0;

    }
    
    if (self.row.clickTitleBtnAction) {
        
        self.row.clickTitleBtnAction(self.row.indexPath);
    }
    
    
}
- (IBAction)clickArrowBtnAction:(UIButton *)sender {
    
     [KeyWindow endEditing:YES];
    
    if (self.row.clickRowAction) {
        
        self.row.clickRowAction(self.row.saveDict[@"id"]);
    }
}

@end
