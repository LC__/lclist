//
//  L2CListWebViewRowView.m
//  L2Cplat
//
//  Created by zhukaiqi on 2017/8/8.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListWebViewRowView.h"

@interface L2CListWebViewRowView ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *active;

@end

@implementation L2CListWebViewRowView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    self.webView.delegate = self;
    
    self.webView.scrollView.bounces = NO;
    
}

- (void)setRow:(L2CListWebViewRow *)row {

    
    _row = row;
    
    [self.active  startAnimating];
    
    dispatch_sync(dispatch_get_global_queue(0, 0), ^{
        
        if (row.url) {
            
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[row.url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
        }
        
        
    });

    

}

- (void)webViewDidFinishLoad:(UIWebView *)webView {

   
    [self.active stopAnimating];
    
    
    if (self.row.webViewDidFinishLoad) {
        
        self.row.webViewDidFinishLoad(webView);
    }


    L2CLog(@"加载完毕");

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    
    [self.active stopAnimating];
    
    L2CLog(@"加载失败啦%@",error);
    
}



@end
