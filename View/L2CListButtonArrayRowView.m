//
//  L2CListButtonArrayRowView.m
//  L2Cplat
//
//  Created by zhukaiqi on 2017/8/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListButtonArrayRowView.h"
#import "L2CListRowVerticalButton.h"
#import "JSBadgeView.h"
@interface L2CListButtonArrayRowView ()

@property (weak, nonatomic) IBOutlet UIView *firstView;

@property (weak, nonatomic) IBOutlet UIView *secondView;

@property (weak, nonatomic) IBOutlet UIView *thirdView;

@property (weak, nonatomic) IBOutlet UIView *lastView;

@property (weak, nonatomic) IBOutlet L2CListRowVerticalButton *firstButton;

@property (weak, nonatomic) IBOutlet L2CListRowVerticalButton *secondButton;

@property (weak, nonatomic) IBOutlet L2CListRowVerticalButton *thirdButton;

@property (weak, nonatomic) IBOutlet L2CListRowVerticalButton *lastButton;

@property (weak, nonatomic) IBOutlet UIView *bottomLine;
@property (weak, nonatomic) IBOutlet UIView *topLine;

@end

@implementation L2CListButtonArrayRowView


- (void)awakeFromNib {

    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;

}


- (void)setRow:(L2CListButtonArrayRow *)row {

    _row = row;
    
    self.firstView.hidden = self.secondView.hidden = self.thirdView.hidden = self.lastView.hidden = NO;
    
    self.topLine.hidden =  !row.isTopSeparatorLine;
    self.bottomLine.hidden = !row.isBottomSeparatorLine;
    switch (row.imagesArray.count) {
        case 0:
        {
            self.firstView.hidden = self.secondView.hidden = self.thirdView.hidden = self.lastView.hidden = YES;
        }
            break;
        case 1:
        {
            self.secondView.hidden = self.thirdView.hidden = self.lastView.hidden = YES;
            
            [self.firstButton setTitle:row.imagesArray[0][@"title"] forState:UIControlStateNormal];
            
            [self.firstButton setImage:[UIImage imageNamed:row.imagesArray[0][@"image"]] forState:UIControlStateNormal];
            
            
        }
            break;
        case 2:
        {
            self.thirdView.hidden = self.lastView.hidden = YES;
            
            [self.firstButton setTitle:row.imagesArray[0][@"title"] forState:UIControlStateNormal];
            
            [self.firstButton setImage:[UIImage imageNamed:row.imagesArray[0][@"image"]] forState:UIControlStateNormal];
            
            [self.secondButton setTitle:row.imagesArray[1][@"title"] forState:UIControlStateNormal];
            
            [self.secondButton setImage:[UIImage imageNamed:row.imagesArray[1][@"image"]] forState:UIControlStateNormal];
        
        }
            break;
        case 3:
        {
            self.lastView.hidden = YES;
            
            [self.firstButton setTitle:row.imagesArray[0][@"title"] forState:UIControlStateNormal];
            
            [self.firstButton setImage:[UIImage imageNamed:row.imagesArray[0][@"image"]] forState:UIControlStateNormal];
            
            [self.secondButton setTitle:row.imagesArray[1][@"title"] forState:UIControlStateNormal];
            
            [self.secondButton setImage:[UIImage imageNamed:row.imagesArray[1][@"image"]] forState:UIControlStateNormal];
            
            [self.thirdButton setTitle:row.imagesArray[2][@"title"] forState:UIControlStateNormal];
            
            [self.thirdButton setImage:[UIImage imageNamed:row.imagesArray[2][@"image"]] forState:UIControlStateNormal];
        }
            break;
        case 4:
        {
            [self.firstButton setTitle:row.imagesArray[0][@"title"] forState:UIControlStateNormal];
            
            [self.firstButton setImage:[UIImage imageNamed:row.imagesArray[0][@"image"]] forState:UIControlStateNormal];
            
            [self.secondButton setTitle:row.imagesArray[1][@"title"] forState:UIControlStateNormal];
            
            [self.secondButton setImage:[UIImage imageNamed:row.imagesArray[1][@"image"]] forState:UIControlStateNormal];
            
            [self.thirdButton setTitle:row.imagesArray[2][@"title"] forState:UIControlStateNormal];
            
            [self.thirdButton setImage:[UIImage imageNamed:row.imagesArray[2][@"image"]] forState:UIControlStateNormal];
            
            [self.lastButton setTitle:row.imagesArray[3][@"title"] forState:UIControlStateNormal];
            
            [self.lastButton setImage:[UIImage imageNamed:row.imagesArray[3][@"image"]] forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
    
    [self addBadgeViewWith:row];

}


- (IBAction)clickButtonViewAction:(UIButton *)sender {
    
    if (self.row.clickButtonIndexAction) {
        
        self.row.clickButtonIndexAction(sender.tag, sender.currentTitle);
    }
    
    switch (sender.tag) {
        case 0:
        {
            self.firstView.backgroundColor = Color(224, 224, 224);
        }
            break;
        case 1:
        {
            self.secondView.backgroundColor = Color(224, 224, 224);
        }
            break;
        case 2:
        {
            self.thirdView.backgroundColor = Color(224, 224, 224);
        }
            break;
        case 3:
        {
            self.lastView.backgroundColor = Color(224, 224, 224);
        }
            break;
            
        default:
            break;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
         self.firstView.backgroundColor = self.lastView.backgroundColor = self.thirdView.backgroundColor = self.secondView.backgroundColor = Color(255, 255, 255);
    });
    
}

- (void)addBadgeViewWith:(L2CListButtonArrayRow *)row {
    
    
    for (NSInteger i= 0; i<row.imagesArray.count; i++) {
        
        NSString *badge = SafeString(row.imagesArray[i][@"badge"]);
        
        switch (i) {
            case 0:
             {
                [self setBadgeViewWith:i view:self.firstButton badge:badge];
              }
                break;
            case 1:
            {
                [self setBadgeViewWith:i view:self.secondButton badge:badge];
            }
                break;
            case 2:
            {
                 [self setBadgeViewWith:i view:self.thirdButton badge:badge];
            }
                break;
            case 3:
            {
               
                [self setBadgeViewWith:i view:self.lastButton badge:badge];
            }
                break;
                
            default:
                break;
        }
        
    }
    

    
    
}

-(void)setBadgeViewWith:(NSInteger )index view:(UIView *)view badge:(NSString *)badge {
    
    JSBadgeView *bageView1 =  [self.firstButton viewWithTag:index+1000];
    
    if (!bageView1) {
        
        bageView1 = [[JSBadgeView alloc] initWithParentView:view alignment:JSBadgeViewAlignmentTopRight];
        
        bageView1.badgeTextFont = [UIFont systemFontOfSize:12];
        
        bageView1.badgePositionAdjustment = CGPointMake(-20, 20);
        
        bageView1.tag = 1000+index;
        
    }
    if (badge.integerValue) {
        
        bageView1.badgeText = badge;
    }
    else {
        [bageView1 removeFromSuperview];
    }
    
}

@end
