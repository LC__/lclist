//
//  L2CListShowMoreHeader.h
//  L2Cplat
//
//  Created by L2Cplat on 2018/9/12.
//  Copyright © 2018年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface L2CListShowMoreHeader : UIView
@property (nonatomic,copy)void (^headerClick)(BOOL isOpen);
/** <#注释#> */
@property (nonatomic,assign) BOOL isOpen;
@end
