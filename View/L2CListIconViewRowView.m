//
//  L2CListIconViewRowView.m
//  L2Cplat
//
//  Created by zhukaiqi on 2017/4/19.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListIconViewRowView.h"
#import "JSBadgeView.h"
#import "UIImage+Extension.h"
@interface L2CListIconViewRowView ()

@property (weak, nonatomic) IBOutlet UIButton *iconView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelWidth;
/** 红点提醒 */
@property (nonatomic,strong)JSBadgeView *badgeView;

@end

@implementation L2CListIconViewRowView

- (JSBadgeView *)badgeView {
   
    if (!_badgeView) {
        
      
        _badgeView = [[JSBadgeView alloc] initWithParentView:self.subtitleLabel alignment:JSBadgeViewAlignmentTopRight];
    
        _badgeView.badgeTextFont = [UIFont systemFontOfSize:12];
        
        _badgeView.badgePositionAdjustment = CGPointMake(-18, 15);
    }
    
    return _badgeView;
}

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    
}

- (void)setRow:(L2CListIconRow *)row {

    _row = row;
    [self.iconView  setImage:row.icon forState:UIControlStateNormal];
    
    
    if (row.imageUrl) {
        
        [self.iconView.imageView sd_setImageWithURL:[NSURL URLWithString:row.imageUrl] placeholderImage:[UIImage imageNamed:@"touxiang"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
           
            if (image) {
                
                [self.iconView setImage:[image circleImage] forState:UIControlStateNormal];
            }
            
            
        }];
    }
    
    if (row.selectedImage) {
        
        [self.iconView setImage:row.selectedImage forState:UIControlStateSelected];

    }
    self.iconView.userInteractionEnabled = row.clickIcon;
   
    self.titleLabel.text = row.title;

    self.subtitleLabel.enabled = NO;
    self.subtitleLabel.text = row.subTitle;
    self.subtitleLabel.placeholder = row.ploText;
    
    self.subtitleLabel.textColor = row.subtitleTextColor?row.subtitleTextColor:Color(102,102,102);
    
    self.arrowImage.hidden = row.isHideArrow;
    
    self.iconView.selected = row.isSelected;
    
    self.titleLabelWidth.constant = [L2CListRowCell fetchTitleLableWidthWithStr:row.title];
    
    if (![SafeString(row.badgeCount) isEqualToString:@"0"]) {

        [self.badgeView setBadgeText:row.badgeCount];
    }
    else {
        
        [self.badgeView removeFromSuperview];
    }

}

- (IBAction)clickIconViewAction:(UIButton *)sender {
    
    self.row.selected = !self.row.isSelected;
    
    if (self.row.clickIconViewAction) {
        self.row.clickIconViewAction(self.row);
    }
}

@end
