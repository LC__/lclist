//
//  RightImageBtn.m
//  L2Cplat
//
//  Created by zhukaiqi on 2017/4/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "RightImageBtn.h"

@implementation RightImageBtn

- (void)setup
{
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.font = [UIFont systemFontOfSize:16];
    self.titleLabel.numberOfLines = 1;
    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    
    [super awakeFromNib];
    
    [self setup];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //button的宽高
    CGFloat buttonWidth = self.width;
    CGFloat buttonHeight = self.height;
    
    // 调整文字
    self.titleLabel.frame = CGRectMake(0,5,buttonWidth-10, buttonHeight-10);
    
    // 调整图片
    self.imageView.x = buttonWidth-10;
    self.imageView.width = 7;
    self.imageView.height = 7;
    self.imageView.y = (self.height-7)/2.0;
    
    
}

@end
