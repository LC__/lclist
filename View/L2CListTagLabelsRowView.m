//
//  L2CListTagLabelsRowView.m
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/8/21.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListTagLabelsRowView.h"
#import "L2CListTagLabelsRow.h"
#import "NotChangeColorLa.h"
#import "NSString+StrSize.h"
#import "L2CImageRightBtn.h"

@interface L2CListTagLabelsRowView ()

@property (weak, nonatomic) IBOutlet UIView *tagBgView;

/**
 tagLabelsArr
 */
@property (nonatomic, strong) NSMutableArray *tagLaArr;

/**
 更多按钮
 */
@property (nonatomic, strong) UIButton *moreBtn;

@end

@implementation L2CListTagLabelsRowView

- (NSMutableArray *)tagLaArr{
    if (!_tagLaArr) {
        _tagLaArr = @[].mutableCopy;
    }
    return _tagLaArr;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
}

- (void)setRow:(L2CListTagLabelsRow *)row {
    

    for (UIView *view in self.tagLaArr) {
        [view removeFromSuperview];
    }

    _row = row;
    
    CGFloat bgWidth = kWidth - 30;
    
    CGFloat curX = 0.0;
    CGFloat curY = 5.0;
    
    NSUInteger rowCount = 1;
    
    
    UILabel *tagLa;
    for (NSDictionary *tagInfo in self.row.tagInfoArr) {
        tagLa = [self creTagLabel];
        tagLa.text = tagInfo[@"tagTitle"];
        
        UIColor *textColor = tagInfo[@"tagTextColor"];
        tagLa.textColor = textColor;
        tagLa.layer.borderColor = textColor.CGColor;
        
        UIColor *bgColor = tagInfo[@"tagBgColor"];
        tagLa.layer.backgroundColor = bgColor.CGColor;
        
        CGFloat width = [tagLa.text sizeWithFont:tagLa.font and:CGSizeMake(MAXFLOAT, tagLa.height)].width + 10;
        width = ceil(MaxEqual(width, kWidth - 40));
        tagLa.width = width;
        
        tagLa.x = curX;
        tagLa.y = curY;
        
        if (CGRectGetMaxX(tagLa.frame) > bgWidth) {
            
            curY = CGRectGetMaxY(tagLa.frame) + 10;
            
            tagLa.y = curY;
            tagLa.x = 0.0;
            
            rowCount ++ ;
            
            if (!row.isExpand && row.rowNum) {
                if (rowCount > row.rowNum) {
                    
                    UIButton *moreBtn = [self creMoreBtn];
                   
                    UILabel *lastTag = self.tagLaArr.lastObject;
                    
                    if (bgWidth - CGRectGetMaxX(lastTag.frame) > (moreBtn.width + 15)) {
                        ;
                    }else{
                        [lastTag removeFromSuperview];
                    }
                    moreBtn.x = bgWidth - (moreBtn.width + 15);
                    moreBtn.y = lastTag.y - 7;
                    
                    [self.tagBgView addSubview:moreBtn];
                    
                    _row.rowHeight = CGRectGetMaxY(moreBtn.frame) + 20;
                    return;
                }
            }
            
        }
        
        curX = CGRectGetMaxX(tagLa.frame) + 8;
        
        [self.tagLaArr addObject:tagLa];
        [self.tagBgView addSubview:tagLa];
    }
    
    _row.rowHeight = CGRectGetMaxY(tagLa.frame) + 20;
    
}


- (UILabel *)creTagLabel{
    
    UILabel *tagLa = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 22)];
    tagLa.font = [UIFont systemFontOfSize:12];
    tagLa.textAlignment = NSTextAlignmentCenter;
    tagLa.layer.cornerRadius = 2;
    tagLa.layer.masksToBounds = YES;
    tagLa.layer.borderWidth = 1;
    
    return tagLa;
}

- (UIButton *)creMoreBtn{
    L2CImageRightBtn *moreBtn = [[L2CImageRightBtn alloc] initWithFrame:CGRectMake(0, 0, 50, 35)];
    [moreBtn setTitle:L2CLocalized(@"public.more", @"Public",@"更多") forState:(UIControlStateNormal)];
    moreBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    moreBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    moreBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    [moreBtn addTarget:self action:@selector(moreClick:) forControlEvents:(UIControlEventTouchUpInside)];
    [moreBtn setTitleColor:CustomTextShowColor forState:(UIControlStateNormal)];
    [moreBtn setImage:[UIImage imageNamed:@"GJSS_icon_xiajiantou"] forState:(UIControlStateNormal)];
    
    moreBtn.imageSize = CGSizeMake(10 , 5);
    return moreBtn;
    
}

- (void)moreClick:(id)sender{
    self.row.isExpand = !self.row.isExpand;
    if (self.row.moreRowAction) {
        self.row.moreRowAction(self.row.indexPath);
    }
    
    [self setRow:self.row];
}

@end
