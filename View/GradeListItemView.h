//
//  GradeListItemView.h
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/8/16.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GradeListItemView : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLa;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subLW;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
