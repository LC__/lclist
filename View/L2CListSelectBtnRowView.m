//
//  L2CListSelectBtnRowView.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListSelectBtnRowView.h"

@interface L2CListSelectBtnRowView ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *mustTagLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UITextField *subtitleTextField;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelWidth;

@property (weak, nonatomic) IBOutlet UITextField *firstTextField;

@property (weak, nonatomic) IBOutlet UITextField *lastTextField;

@property (weak, nonatomic) IBOutlet UIButton *firstButton;
@property (weak, nonatomic) IBOutlet UIButton *lastButton;

@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateViewHeight;

@end

@implementation L2CListSelectBtnRowView


-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;

    self.bgViewHeight.constant = 0.5;
    
    self.subtitleTextField.delegate = self;
    
    self.firstTextField.delegate = self;
    
    self.lastTextField.delegate = self;
    
    self.firstTextField.tag = 1001;
    
    self.lastTextField.tag = 1002;
    
}

- (void)setRow:(l2CListSelectBtnRow *)row {
    
    _row = row;
    
    self.mustTagLabel.hidden = !row.isMustTag;
    
    self.titleLabel.text = row.title;
    
    self.subtitleTextField.userInteractionEnabled = row.isInput;
    
    self.subtitleTextField.text = row.subTitle3;
    
    if (row.isHiddenDate) {
        
        self.dateViewHeight.constant = 0;
        self.dateView.hidden = YES;
    }
    else {
        
        self.dateViewHeight.constant = 34;
        self.dateView.hidden = NO;
        
    }
    
    self.subtitleTextField.keyboardType = row.keyboardType?row.keyboardType:UIKeyboardTypeDefault;
    self.subtitleTextField.textColor = row.subtitleTextColor?row.subtitleTextColor:Color(102,102,102);
    
    if(row.titleTextColor){
        
        self.titleLabel.textColor = row.titleTextColor;

    }else{
        
        self.titleLabel.textColor = [UIColor blackColor];
    }
        
    if (row.isBecomeFirstResponder) {
        
        [self.subtitleTextField becomeFirstResponder];

    }
    
    if (row.placeHolder) {
        
        self.subtitleTextField.placeholder = row.placeHolder;
    }
    else {
    
        if (!row.isMustTag) {
            
            self.subtitleTextField.placeholder = L2CLocalized(@"public.Optional field", @"Public",@"选填");
        }
        else {
            self.subtitleTextField.placeholder = L2CLocalized(@"public.Required field", @"Public",@"必填");
            
        }
    
    }
    
    
    if (row.isMultiple) {
        
        NSArray *array = [row.subTitle2 componentsSeparatedByString:@","];
        
        [self setMultipleBtnLayoutSubViewsWith:row.array tags:array];
        
        NSArray *titleArray = [row.subTitle componentsSeparatedByString:@","];
        
        
        if (![row.subTitle isEqualToString:@""]) {
            
            if ((array.count==titleArray.count)&&array.count) {
                
                row.saveDict  = [NSMutableDictionary dictionaryWithObjects:titleArray forKeys:array];
            }
        }
        else {
        
            for (NSString *key in array) {
                
                if (![key isEqualToString:@""]) {
                    
                      [row.saveDict setObject:@"" forKey:key];
                }

            }
        }
        

        
    }
    else {
        [self setSigleBtnLayoutSubViewsWith:row.array tags:row.subTitle2];

    }
    
    self.titleLabelWidth.constant = [L2CListRowCell fetchTitleLableWidthWithStr:row.title];
    
    self.firstTextField.text = row.subTitle4;
    
    self.lastTextField.text = row.subTitle5;
    
    //设置占位字符
    if (row.firstPlaceHolder) {
        
        self.firstTextField.placeholder = row.firstPlaceHolder;
        self.lastTextField.placeholder = row.lastPlaceHolder;
        
    }
    else{
        
        if (!row.isMustTag) {
            
            self.firstTextField.placeholder = L2CLocalized(@"public.Optional field", @"Public",@"选填");
            self.lastTextField.placeholder = L2CLocalized(@"public.Optional field", @"Public",@"选填");
        }
        else {
            self.firstTextField.placeholder = L2CLocalized(@"public.Required field", @"Public",@"必填");
            self.lastTextField.placeholder = L2CLocalized(@"public.Required field", @"Public",@"必填");
            
        }
    }
    
}


//添加button
- (void)setSigleBtnLayoutSubViewsWith:(NSArray *)array tags:(NSString *)tags {
    
    //添加前 先移除所有视图
    for (UIView *view in self.bgView.subviews) {
        
        if (view.tag != 9999) {
            
            [view removeFromSuperview];
        }
        
    }
    
    //bt的个数
    NSInteger btCount = array.count;
        
    //bt的长宽高
    CGFloat btX = 15;
    CGFloat btY = 0;
    CGFloat btW = (self.frame.size.width - 3*5 - 15-10 )/4.0;
    CGFloat btH =  34;
    CGFloat marginTop = 10;
        
    if ( btCount) {
        
       NSInteger ro = (btCount-1)/4;
        
       for (NSInteger i = 0; i<btCount; i++) {
        //bt的行
        NSInteger row = i/4;
        //bt的列
        NSInteger col = i%4;
                
        NSString *title = array[i][@"value"];
        
        NSString *tagId = array[i][@"id"];
        
        UIButton *bt = [UIButton buttonWithType:UIButtonTypeCustom];
           
        bt.titleLabel.numberOfLines = 0;
        bt.titleLabel.textAlignment = NSTextAlignmentCenter;
//           if (title.length > 5) {
//
//               NSString *str1 = [title substringToIndex:3];
//               NSString *str2 = [title substringFromIndex:3];
//               title = [NSString stringWithFormat:@"%@\n%@",str1,str2];
//           }
           
        bt.tagId = tagId;
           
        bt.frame = CGRectMake(btX+(5+btW)*col, btY+(marginTop+btH)*row, btW, btH);
        [bt setTitle:title forState:UIControlStateNormal];
    
        //若果选中时
        if ([tags isEqualToString:tagId] && ![tags isEqualToString:@""]) {
                    
        [bt setTitleColor:[UIColor colorWithRed:0.1059 green:0.3725 blue:0.8431 alpha:1.0] forState:UIControlStateNormal];
        [bt setBackgroundImage:[UIImage imageNamed:@"SX_bg_xuanxiang_"] forState:UIControlStateNormal];
                    
        }
        else {
                   
        [bt setTitle:title forState:UIControlStateNormal];
        [bt setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        bt.backgroundColor =   Color(238, 238, 238);
                }
        bt.titleLabel.font = [UIFont systemFontOfSize:12];
        [bt addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        bt.tagId =tagId;
        bt.layer.cornerRadius = 3;
        bt.layer.masksToBounds = YES;
        [self.bgView  addSubview:bt];
           
        }
        self.bgViewHeight.constant = (ro+1)*(btH+marginTop);
        
        
        if (self.row.isHiddenDate == NO) {
            
           self.bgViewHeight.constant = (ro+1)*(btH+marginTop)+44;
            
         }
       
         self.row.rowHeight = self.bgViewHeight.constant+57;
        
            
    }
        
 
}


//多选按钮

//添加button
- (void)setMultipleBtnLayoutSubViewsWith:(NSArray *)array tags:(NSArray *)tags {
    
    //添加前 先移除所有视图
    for (UIView *view in self.bgView.subviews) {
        
        if (view.tag != 9999) {
            
            [view removeFromSuperview];
        }
    }
    //bt的个数
    NSInteger btCount = array.count;
    
    //bt的长宽高
    CGFloat btX = 15;
    CGFloat btY = 0;
    CGFloat btW = (self.frame.size.width - 3*5 - 15-10 )/4.0;
    CGFloat btH =  34;
    CGFloat marginTop = 10;
    
    if ( btCount) {
        
        NSInteger ro = (btCount-1)/4;
        
        for (NSInteger i = 0; i<btCount; i++) {
            
            //bt的行
            NSInteger row = i/4;
            
            //bt的列
            NSInteger col = i%4;
            
            NSString *title = array[i][@"value"];
            NSString *tagId = array[i][@"id"];
            UIButton *bt = [UIButton buttonWithType:UIButtonTypeCustom];
        
            bt.titleLabel.numberOfLines = 0;
            bt.titleLabel.textAlignment = NSTextAlignmentCenter;
//            if (title.length > 5) {
//
//                NSString *str1 = [title substringToIndex:3];
//                NSString *str2 = [title substringFromIndex:3];
//                title = [NSString stringWithFormat:@"%@\n%@",str1,str2];
//            }
        
            bt.frame = CGRectMake(btX+(5+btW)*col, btY+(marginTop+btH)*row, btW, btH);
            
            for (NSString *tg in tags) {
                
                if (![tg isEqualToString:@""]) {
                    
                    if ([tg isEqualToString:tagId]) {
                        
                        bt.selected = !bt.selected;
                    }
                }
            }
            
            
            [bt setTitle:title forState:UIControlStateNormal];
            [bt setTitleColor:[UIColor colorWithRed:0.1059 green:0.3725 blue:0.8431 alpha:1.0] forState:UIControlStateSelected];
            [bt setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [bt setBackgroundImage:[UIImage imageNamed:@"SX_bg_xuanxiang_"] forState:UIControlStateSelected];
            [bt setBackgroundImage:[UIImage imageNamed:@"bz_bg_moren"] forState:UIControlStateNormal];
           
            bt.titleLabel.font = [UIFont systemFontOfSize:12];
            [bt addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
            bt.tagId =tagId;
            bt.layer.cornerRadius = 3;
            bt.layer.masksToBounds = YES;
            [self.bgView  addSubview:bt];
            
        }
        self.bgViewHeight.constant = (ro+1)*(btH+marginTop);
        
    }
    
    
}

- (void)clickAction:(UIButton *)sender {

     [KeyWindow endEditing:YES];
    
    NSString *selectTitle = [sender.titleLabel.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    NSString *selectId = sender.tagId;
    
    NSString *text = self.subtitleTextField.text;
    
    sender.selected = !sender.selected;
    
    if (self.row.isMultiple) {
        
        if (sender.selected) {
            
            [self.row.saveDict setObject:selectTitle forKey:selectId];
        }
        else {
        
            [self.row.saveDict removeObjectForKey:selectId];
        }
        
        NSArray *allValues = self.row.saveDict.allValues;
        NSArray *allKeys = self.row.saveDict.allKeys;
        NSMutableString *selectTitleStr = [NSMutableString string];
        NSMutableString *selectIdStr = [NSMutableString string];
        [self setMultipleBtnLayoutSubViewsWith:self.row.array tags:allKeys];
        
        for (NSString *str in allValues) {
            
            [selectTitleStr appendFormat:@"%@,", str];
        }
        for (NSString *str in allKeys) {
            
            [selectIdStr appendFormat:@"%@,", str];
        }
        self.row.subTitle =[selectTitleStr substringToIndex:selectTitleStr.length-1];
        
        self.row.subTitle2 = [selectIdStr substringToIndex:selectIdStr.length-1];
        
    }
    else {
    
        self.row.subTitle = selectTitle;
        
        self.row.subTitle2 = selectId;
        
        self.row.saveDict = [@{@"id":selectId,@"value":selectTitle,@"text":SafeString(text)} mutableCopy];
        
        [self setSigleBtnLayoutSubViewsWith:self.row.array tags:selectId];
    }
    
    if (self.row.clickRowAction) {
        
        self.row.clickRowAction(self.row.saveDict);
    }
  
    L2CLog(@"%@",self.row.saveDict);

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    return [textField resignFirstResponder];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    [self performSelector:@selector(setText:) withObject:textField afterDelay:0.03];
    
    return YES;
    
}


- (void)setText:(UITextField *)textField{
    
    
    if (self.row.textInPutAfterBlock) {
        
        self.row.textInPutAfterBlock(textField);
    }
    
    self.row.subTitle3 = textField.text;
    
    
}


- (IBAction)clickFirstButtonAction:(UIButton *)sender {
    
    
    if (self.row.isInput==NO) {
        
        [KeyWindow endEditing:YES];
    }
    
    if (self.row.clickCustomDateView) {
        
        self.row.clickCustomDateView(@"first");
        
    }
    
    L2CLog(@"%s",__func__);
}

- (IBAction)clickLastButtonAction:(UIButton *)sender {
    
    
    if (self.row.isInput==NO) {
        
        [KeyWindow endEditing:YES];
    }
    
    if (self.row.clickCustomDateView) {
        
        self.row.clickCustomDateView(@"last");
        
    }
    
    L2CLog(@"%s",__func__);
}



@end
