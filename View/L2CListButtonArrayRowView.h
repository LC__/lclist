//
//  L2CListButtonArrayRowView.h
//  L2Cplat
//
//  Created by zhukaiqi on 2017/8/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "L2CListRowCell.h"
@interface L2CListButtonArrayRowView : UIView

/** 多个按钮的row */
@property (nonatomic,strong)L2CListButtonArrayRow *row;

@end
