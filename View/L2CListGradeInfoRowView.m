//
//  L2CListGradeInfoRowView.m
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/8/16.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListGradeInfoRowView.h"
#import "GradeListItemView.h"

@implementation L2CListGradeInfoRowView


- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
  
}
- (void)setRow:(L2CListGradeInfoRow *)row{
    _row = row;
    
    [self setGradeItems];
    
}

- (void)setGradeItems{
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    CGFloat curY = 0.0;
    for (NSDictionary *itemInfo in self.row.gradeInfoArr) {
        GradeListItemView *item = [GradeListItemView loadViewFromXib];
        item.frame = CGRectMake(0, 0, kWidth, 40);
        
        item.titleLa.text = itemInfo[@"title"];
        item.subTitleLa.text = [NSString stringWithFormat:@"%@\t",itemInfo[@"subTitle"]];
        item.subTitleLa.backgroundColor = itemInfo[@"bgColor"];
        item.subLW.constant = ([itemInfo[@"persent"] floatValue] / 100.0) * item.bgView.width;
        item.y = curY;
        
        [self addSubview:item];
        curY = CGRectGetMaxY(item.frame);
    }
    _row.rowHeight = curY;
}

@end
