//
//  L2CListArrowRowView.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListArrowRowView.h"

@interface L2CListArrowRowView ()
@property (weak, nonatomic) IBOutlet UILabel *mustTagLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UITextField *subtitleTextField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelWidth;
@property (weak, nonatomic) IBOutlet UIImageView *jiantou;

@end

@implementation L2CListArrowRowView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    
}

- (void)setRow:(L2CListArrowRow *)row {
    
    self.mustTagLabel.hidden = !row.isMustTag;
    
    self.titleLabel.text = row.title;
    
    if (row.titleFont) {
        self.titleLabel.font = row.titleFont;
    }
    
    if (row.subTitleFont) {
        self.subtitleTextField.font = row.subTitleFont;
    }
    
    self.subtitleTextField.text = row.subTitle;
    
    if(row.titleTextColor){
        
        self.titleLabel.textColor = row.titleTextColor;
        
    }else{
        
        self.titleLabel.textColor = [UIColor blackColor];
    }
    
    self.subtitleTextField.textColor = row.subtitleTextColor?row.subtitleTextColor:Color(102,102,102);
    
    if (row.placeHolder) {
        
        self.subtitleTextField.placeholder = row.placeHolder;
    }
    else {
        
        if (!row.isMustTag) {
            
            self.subtitleTextField.placeholder = L2CLocalized(@"public.Optional field", @"Public",@"选填");
        }
        else {
            
            self.subtitleTextField.placeholder = L2CLocalized(@"public.Required field", @"Public",@"必填");
            
        }
    
    }
    
    if (row.isHiddenPlaceHolder) {
        
         self.subtitleTextField.placeholder = @"";   
    }
   
    
    self.titleLabelWidth.constant = [L2CListRowCell fetchTitleLableWidthWithStr:row.title];
    
    self.jiantou.hidden = row.isHideJiantou;

}

@end
