//
//  L2CListProgressRowView.m
//  L2Cplat
//
//  Created by feaonline on 2017/8/16.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListProgressRowView.h"
#import "SharpContentView.h"
#import "UIImage+Extension.h"

@interface L2CListProgressRowView ()

/** 状态image */
@property (weak, nonatomic) IBOutlet UIImageView *progressImage;
/** 头像 */
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
/** 状态 */
@property (weak, nonatomic) IBOutlet UILabel *statusLavel;
/** 内容 */
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
/** 绘制尖角view */
@property (weak, nonatomic) IBOutlet SharpContentView *progressView;
/** 人名 */
@property (weak, nonatomic) IBOutlet UILabel *approverName;
/** 时间 */
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
/** 附件 */
@property (weak, nonatomic) IBOutlet UIImageView *attachmentImage;
/** 附件个数 */
@property (weak, nonatomic) IBOutlet UILabel *fileNum;
/** 分割线 */
@property (weak, nonatomic) IBOutlet UILabel *lineLabel;

@end

@implementation L2CListProgressRowView


- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
}


- (void)setRow:(L2CListProgressRow *)row {
    
    _row = row;
    
    self.progressView.sharpY = 15;
    
    self.progressImage.image = row.statusImage;
    
    [self.headImage sd_setImageWithURL:[NSURL URLWithString:row.approverImagePath] placeholderImage:[UIImage imageNamed:@"male"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            
            self.headImage.image = [image circleImage];
            
        }else{
            
            self.headImage.image = [UIImage imageNamed:@"male"];
        }
    }];
    
    self.timeLabel.text = row.approverTime;
    
    self.statusLavel.text = row.approverStatus;
    
    self.approverName.text = row.approverName;
    
    if(row.fileNum != 0 && row.fileNum){
        
        self.attachmentImage.hidden = NO;
        
        self.fileNum.hidden = NO;
        
        self.fileNum.text = [NSString stringWithFormat:@"%ld",row.fileNum];
        
    }else{
    
        self.attachmentImage.hidden = YES;
        
        self.fileNum.hidden = YES;
    }
    
    
    self.contentLabel.text = row.remark;
    
    if(row.remark.length > 0)
    {
        self.contentLabel.hidden = NO;
        
        self.lineLabel.hidden = NO;
        
    }else{
        
        self.contentLabel.hidden = YES;
        
        self.lineLabel.hidden = YES;
        
    }
    
    self.row.rowHeight = [self rowHeight];

}

-(CGFloat)rowHeight{

    CGSize size = CGSizeMake(ceil(kWidth - 80), MAXFLOAT);
    
    CGSize stringSize = [self.contentLabel.text boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:self.contentLabel.font} context:nil].size;
    
    CGFloat height = ceil(stringSize.height+90);
    
    if([self.contentLabel.text isEqualToString:@""] || self.contentLabel.text == nil)
    {
        height = 70;
    
    }
    
    return height;
}

@end
