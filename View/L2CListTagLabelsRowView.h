//
//  L2CListTagLabelsRowView.h
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/8/21.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "L2CListRowCell.h"
@interface L2CListTagLabelsRowView : UIView


@property (nonatomic, strong) L2CListTagLabelsRow *row;
@end
