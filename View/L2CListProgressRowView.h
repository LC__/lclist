//
//  L2CListProgressRowView.h
//  L2Cplat
//
//  Created by feaonline on 2017/8/16.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "L2CListRowCell.h"
@interface L2CListProgressRowView : UIView

/**
 行模型
 */
@property (nonatomic, strong) L2CListProgressRow *row;

@end
