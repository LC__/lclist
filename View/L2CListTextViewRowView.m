//
//  L2CListTextViewRowView.m
//  L2Cplat
//
//  Created by zhukaiqi on 2017/5/9.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListTextViewRowView.h"

@interface L2CListTextViewRowView ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (weak, nonatomic) IBOutlet UILabel *placeHolderLabel;

@property (weak, nonatomic) IBOutlet UILabel *totalwordsLabel;


@end

@implementation L2CListTextViewRowView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    self.textView.delegate = self;
    
    
}

- (void)setRow:(L2CListTextViewRow *)row {
    
    
    _row = row;
    
    self.placeHolderLabel.text = row.placeHolder;
    
    self.textView.text = row.subTitle;
    
    self.placeHolderLabel.hidden = row.subTitle.length;
    
    self.textView.editable = !row.isNoEditor;

    if (row.isBecomeFirstResponder && !row.isNoEditor) {
        
        [self.textView becomeFirstResponder];

    }
    
    self.totalwordsLabel.text = [NSString stringWithFormat:@"%zd/%ld",row.subTitle.length,self.row.maxWords];

    self.totalwordsLabel.hidden = !row.maxWords;
}

- (void)textViewDidChange:(UITextView *)textView {
    
    
    self.placeHolderLabel.hidden = textView.text.length;
    
    
    if (textView.text.length>=self.row.maxWords && self.row.maxWords>0) {
        
        textView.text = [textView.text substringToIndex:self.row.maxWords];
    }
    
    self.row.subTitle = textView.text;

    
    self.totalwordsLabel.text = [NSString stringWithFormat:@"%zd/%ld",textView.text.length,self.row.maxWords];
    
    self.totalwordsLabel.hidden = !self.row.maxWords;

    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    self.placeHolderLabel.hidden = textView.text.length;
    
    self.totalwordsLabel.hidden = !self.row.maxWords;
    
    self.totalwordsLabel.text = [NSString stringWithFormat:@"%zd/%ld",textView.text.length,self.row.maxWords];
    
    return YES;
    
}

@end
