//
//  L2CListSalaryRowView.m
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/11/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListSalaryRowView.h"
#import "NSString+IntegerShow.h"
#import "UIView+BMLine.h"
@interface L2CListSalaryRowView ()

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UILabel *totalLa;
@property (weak, nonatomic) IBOutlet UILabel *line;


/** 内容宽度 */
@property(nonatomic, assign) CGFloat infoW;

/** 21/0 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vLeading;

/** 20/0 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vBottom;

@property (weak, nonatomic) IBOutlet UILabel *tLine;
@property (weak, nonatomic) IBOutlet UILabel *bLine;


@property (weak, nonatomic) IBOutlet UILabel *sep1;
@property (weak, nonatomic) IBOutlet UILabel *sep2;
@end
@implementation L2CListSalaryRowView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    self.bgView.layer.borderWidth = 0.5;
    self.bgView.layer.cornerRadius = 4;

}

- (void)setRow:(L2CListSalaryRow *)row{
    _row = row;
    self.totalLa.text = [row.totalNum integerShow];
    [self configLine];
    [self setInfoList];
    [self layoutIfNeeded];
    [self drawDash];
    [self layoutIfNeeded];
}

- (void)drawDash{
    [self.sep1 addLineWithType:BMLineTypeCustomDash color:[UIColor orangeColor] position:BMLinePostionCustomTop];
    [self.sep2 addLineWithType:BMLineTypeCustomDash color:[UIColor orangeColor] position:BMLinePostionCustomTop];
}

- (void)configLine{
    self.line.hidden = !self.row.isShowLine;
    self.tLine.hidden = self.row.isShowLine;
    self.bLine.hidden = self.row.isShowLine;
    
    
    if (self.row.isShowLine) {
        self.vLeading.constant = 30.0;
        self.vTrailing.constant = 20.0;
        
        self.bgView.layer.borderColor = [UIColor orangeColor].CGColor;
        self.infoW = kWidth - 41 - 30;
        self.vBottom.constant = 15.0;
    }else{
        self.vLeading.constant = 0.0;
        self.vTrailing.constant = 0.0;
        self.infoW = kWidth - 30;
        self.bgView.layer.borderColor = [UIColor clearColor].CGColor;
        self.vBottom.constant = 0.0;
    }
   
}

- (void)setInfoList{
    
    for (UIView *view in self.infoView.subviews) {
        [view removeFromSuperview];
    }
    
    CGFloat curX = 0.0;
    CGFloat curY = -5;
    
    for (NSMutableDictionary *info in self.row.itemArr) {
        
        UIButton *titleLa = [self creTitleLabel];
        [titleLa setTitle:info[@"title"] forState:(UIControlStateNormal)];
        
        titleLa.x = curX;
        titleLa.y = curY + 8;
        
        UIButton *infoLa = [self creInfoLabel];
        [infoLa setTitle:info[@"info"] forState:(UIControlStateNormal)];
        
        infoLa.x = CGRectGetMaxX(titleLa.frame) + 5;
        infoLa.y = curY + 8;
        CGFloat infoH = [info[@"height"] floatValue];
        
        if (!infoH) {
            infoH = ceil([infoLa.titleLabel.text sizeWithFont:infoLa.titleLabel.font and:CGSizeMake(ceil(infoLa.width), MAXFLOAT)].height + 2);
            info[@"height"] = @(infoH);
        }
 
        infoH = MinEqual(infoH, 20);
        infoLa.height = infoH;
        
        curY = CGRectGetMaxY(infoLa.frame);
        
        [self.infoView addSubview:titleLa];
        [self.infoView addSubview:infoLa];
    }
    _row.rowHeight = curY + 85 + self.vBottom.constant;
}


- (UIButton *)creTitleLabel{
    
    UIButton *title = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 65, 20)];
    title.userInteractionEnabled = NO;
    title.titleLabel.font = [UIFont systemFontOfSize:14];
    title.titleLabel.numberOfLines = 0;
    
    [title setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    title.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    title.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    return title;
}

- (UIButton *)creInfoLabel{
    
    UIButton *info = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, ceil(self.infoW - 70), 20)];
    info.userInteractionEnabled = NO;
    info.titleLabel.font = [UIFont systemFontOfSize:14];
    info.titleLabel.numberOfLines = 0;
    
    [info setTitleColor:[UIColor orangeColor] forState:(UIControlStateNormal)];
    info.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    info.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    return info;
}

@end
