//
//  L2CListIndicatorRowView.m
//  L2Cplat
//
//  Created by feaonline on 2017/8/8.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListIndicatorRowView.h"

@interface L2CListIndicatorRowView()


@property (weak, nonatomic) IBOutlet UIImageView *imageView;

/** title */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation L2CListIndicatorRowView


- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
}


- (void)setRow:(L2CListIndicatorRow *)row {

    _row = row;
    
    self.imageView.image = row.icon;
    
    self.titleLabel.text = row.title;
    
    if(row.textFont){
    
        self.titleLabel.font = [UIFont systemFontOfSize:row.textFont];
    }
}



@end
