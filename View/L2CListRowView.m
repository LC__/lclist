//
//  L2CListRowView.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRowView.h"

@interface L2CListRowView ()
@property (weak, nonatomic) IBOutlet UILabel *mustTagLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UITextField *subtitleTextField;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelWidth;


@end

@implementation L2CListRowView

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    
}

- (void)setRow:(L2CListDefultRow *)row {

    self.mustTagLabel.hidden = !row.isMustTag;
    
    self.titleLabel.text = row.title;
    if (row.titleFont) {
        self.titleLabel.font = row.titleFont;
    }
    if (row.subTitleFont) {
        self.subtitleTextField.font = row.subTitleFont;
    }
    if (row.titleTextColor) {
        self.titleLabel.textColor = row.titleTextColor;
    }
    
    self.subtitleTextField.text = row.subTitle;
    
    self.subtitleTextField.textColor = row.subtitleTextColor?row.subtitleTextColor:Color(102,102,102);
    
    self.titleLabelWidth.constant = [L2CListRowCell fetchTitleLableWidthWithStr:row.title];

}

@end
