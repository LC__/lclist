//
//  L2CListRowCell.m
//  基类cell模型
//
//  Created by zhukaiqi on 2017/4/6.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import "L2CListRowCell.h"
#import "L2CListRowView.h"
#import "L2CListArrowRowView.h"
#import "L2CListSwitchRowView.h"
#import "UIView+UIViewExtension.h"
#import "L2CListTextFieldRowView.h"
#import "L2CListSelectBtnRowView.h"
#import "L2CListSelectBtnAndArrowRowView.h"
#import "L2CListIconViewRowView.h"
#import "L2CListTextViewRowView.h"
#import "L2CListStarViewRowView.h"
#import "L2CListContentViewRowView.h"
#import "L2CListTwoTextFieldRowView.h"
#import "L2CListMultiRowTextView.h"
#import "L2CListButtonArrayRowView.h"
#import "L2CListIndicatorRowView.h"
#import "L2CListWebViewRowView.h"
#import "L2CListLineInfoView.h"
#import "L2CListGradeInfoRowView.h"
#import "L2CListProgressRowView.h"
#import "L2CListTagLabelsRowView.h"
#import "L2CListSalaryRowView.h"
#import "L2CIndContentRowView.h"
#import "L2CListEchartsListRowView.h"


@interface L2CListRowCell ()

/** 默认行视图 */
@property (nonatomic,strong)L2CListRowView *rowView;

/** 箭头行视图 */
@property (nonatomic,strong)L2CListArrowRowView *arrowView;

/** 开关视图 */
@property (nonatomic,strong)L2CListSwitchRowView *switchView;

/** 输入框视图 */
@property (nonatomic,strong)L2CListTextFieldRowView *textFieldView;

/** 单选按钮视图 */
@property (nonatomic,strong)L2CListSelectBtnRowView *btnRowView;

/** 单选标题并且可以选择的视图 */
@property (nonatomic,strong)L2CListSelectBtnAndArrowRowView *btnAndArrowRowView;

/** 头像视图 */
@property (nonatomic,strong)L2CListIconViewRowView *iconRowView;

/** 文本框视图 */
@property (nonatomic,strong)L2CListTextViewRowView *textViewRowView;

//** 星星视图 */
@property (nonatomic,strong)L2CListStarViewRowView *starViewRowView;

//** 内容视图 */
@property (nonatomic,strong) L2CListContentViewRowView *contentViewRowView;

/** 两个输入框视图 */
@property (nonatomic,strong)L2CListTwoTextFieldRowView *twoTextFieldRowView;

/** 多行内容显示视图 */
@property (nonatomic, strong)L2CListMultiRowTextView *multiRowTextView;

/** 多个按钮的视图 */
@property (nonatomic,strong)L2CListButtonArrayRowView *buttonArrayView;

/** 指示视图 */
@property (nonatomic,strong) L2CListIndicatorRowView *indicatorRowView;

/** webview视图 */
@property (nonatomic,strong)L2CListWebViewRowView *webViewRowView;

/** webview视图 */
@property (nonatomic,strong)L2CListEchartsListRowView *chartsRowView;

/**
 自定义视图
 */
@property (nonatomic, strong) UIView *customView;

/**
 流程视图
 */
@property (nonatomic, strong) L2CListLineInfoView *lineinfoRowView;

/**
 熟练度视图
 */
@property (nonatomic, strong) L2CListGradeInfoRowView *gradeInfoRowView;

/**
 审批流程
 */
@property (nonatomic, strong) L2CListProgressRowView *progressRowView;

/**
 文字标签视图
 */
@property (nonatomic, strong) L2CListTagLabelsRowView *tagLabelsRowView;

/**
 薪水视图
 */
@property (nonatomic, strong) L2CListSalaryRowView *salaryRowView;

@property (nonatomic, strong) L2CIndContentRowView *indContentView;

@end


@implementation L2CListRowCell


/**
 懒加载
 
 @return 懒加载
 */
- (L2CListRowView *)rowView {
    
    if (!_rowView) {
        
         _rowView = [L2CListRowView loadViewFromXib];
        
    }
    
    
    return _rowView;
    
    
}

/**
 懒加载
 
 @return 懒加载
 */
- (L2CListArrowRowView *)arrowView {
    
    if (!_arrowView) {
        
        _arrowView = [L2CListArrowRowView loadViewFromXib];
    
    }
    
    
    return _arrowView;
    
    
}
/**
 懒加载
 
 @return 懒加载
 */
- (L2CListSwitchRowView *)switchView {
    
    if (!_switchView) {
        
        _switchView = [L2CListSwitchRowView loadViewFromXib];
        
    }
    
    
    return _switchView;
    
    
}

/**
 懒加载
 
 @return 懒加载
 */
- (L2CListTextFieldRowView *)textFieldView {
    
    if (!_textFieldView) {
        
        _textFieldView = [L2CListTextFieldRowView loadViewFromXib];
        

    }
    
    
    return _textFieldView;
    
    
}

/**
 懒加载
 
 @return 懒加载
 */
- (L2CListSelectBtnRowView *)btnRowView {
    
    if (!_btnRowView) {
        
        _btnRowView = [L2CListSelectBtnRowView loadViewFromXib];
        
    }
    
    
    return _btnRowView;
    
    
}

/**
 懒加载
 
 @return 懒加载
 */
- (L2CListSelectBtnAndArrowRowView *)btnAndArrowRowView {
    
    if (!_btnAndArrowRowView) {
        
        _btnAndArrowRowView = [L2CListSelectBtnAndArrowRowView loadViewFromXib];
        

    }
    
    
    return _btnAndArrowRowView;
    
    
}

/**
 懒加载
 
 @return 懒加载
 */
- (L2CListIconViewRowView *)iconRowView {
    
    if (!_iconRowView) {
        
        _iconRowView = [L2CListIconViewRowView loadViewFromXib];
        

    }
    

    
    return _iconRowView;
    
    
}


/**
 懒加载
 
 @return 懒加载
 */
- (L2CListTextViewRowView *)textViewRowView {
    
    if (!_textViewRowView) {
        
        _textViewRowView = [L2CListTextViewRowView loadViewFromXib];
        
    }
   
    
    
    return _textViewRowView;
    
    
}
/**
 懒加载
 
 @return 懒加载
 */
- (L2CListStarViewRowView *)starViewRowView {


    if (!_starViewRowView) {
        
        _starViewRowView = [L2CListStarViewRowView loadViewFromXib];

    }
    
    return _starViewRowView;
    
   

}
/**
 懒加载
 
 @return 懒加载
 */
- (L2CListContentViewRowView *)contentViewRowView {

    if (!_contentViewRowView) {
        
        _contentViewRowView = [L2CListContentViewRowView loadViewFromXib];
        
    }
    
    return _contentViewRowView;

}
/**
 懒加载
 
 @return 懒加载
 */
- (L2CListTwoTextFieldRowView *)twoTextFieldRowView {
    
    if (!_twoTextFieldRowView) {
        
        _twoTextFieldRowView = [L2CListTwoTextFieldRowView loadViewFromXib];
        
    }
    
    return _twoTextFieldRowView;
    
}

/**
 懒加载

 @return 懒加载
 */
- (L2CListMultiRowTextView *)multiRowTextView {
    
    if (!_multiRowTextView) {
        _multiRowTextView = [L2CListMultiRowTextView loadViewFromXib];
    }
    
    return _multiRowTextView;
}

/**
 懒加载
 
 @return 懒加载
 */
- (L2CListButtonArrayRowView *)buttonArrayView {
    
    if (!_buttonArrayView) {
        
        _buttonArrayView = [L2CListButtonArrayRowView loadViewFromXib];
    }
    
    
    return _buttonArrayView;
    
    
}


/**
 懒加载
 
 @return 懒加载
 */
- (L2CListIndicatorRowView *)indicatorRowView {
    
    if (!_indicatorRowView) {
        
        _indicatorRowView = [L2CListIndicatorRowView loadViewFromXib];
        
    }
    
    return _indicatorRowView;
    
}


/**
 懒加载
 
 @return 懒加载
 */
- (L2CListWebViewRowView *)webViewRowView {
    
    if (!_webViewRowView) {
        
        _webViewRowView = [L2CListWebViewRowView loadViewFromXib];
    }
    
    
    return _webViewRowView;
}

/**
 懒加载
 
 @return 懒加载
 */
- (L2CListProgressRowView *)progressRowView {
    
    if (!_progressRowView) {
        
        _progressRowView = [L2CListProgressRowView loadViewFromXib];
        
    }
    
    
    return _progressRowView;

}

/**
 懒加载
 
 @return 懒加载
 */
- (L2CListLineInfoView *)lineinfoRowView{
    if (!_lineinfoRowView) {
        _lineinfoRowView = [L2CListLineInfoView loadViewFromXib];
    }
    return _lineinfoRowView;
}

- (L2CListGradeInfoRowView *)gradeInfoRowView{
    if (!_gradeInfoRowView) {
        _gradeInfoRowView = [L2CListGradeInfoRowView loadViewFromXib];
    }
    return _gradeInfoRowView;
}

- (L2CListTagLabelsRowView *)tagLabelsRowView{
    if (!_tagLabelsRowView) {
        _tagLabelsRowView = [L2CListTagLabelsRowView loadViewFromXib];
    }
    return _tagLabelsRowView;
}

- (L2CListSalaryRowView *)salaryRowView{
    if (!_salaryRowView) {
        _salaryRowView = [L2CListSalaryRowView loadViewFromXib];
    }
    return _salaryRowView;
}
- (L2CIndContentRowView *)indContentView{
    if(!_indContentView){
        _indContentView = [L2CIndContentRowView loadViewFromXib];
    }
    return _indContentView;
}

- (L2CListEchartsListRowView *)chartsRowView{
    if(!_chartsRowView){
        _chartsRowView = [L2CListEchartsListRowView loadViewFromXib];
    }
    return _chartsRowView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (instancetype)cellWithTableView:(UITableView *)tableView andIdentifier:(NSString *)identifier{

//        static NSString *identifier = @"L2CListRowCell";
    
    
        L2CListRowCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (cell == nil) {
            cell = [[L2CListRowCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
    
        return cell;

}

- (void)setRow:(L2CListRow *)row {

    _row = row;
   
    for (UIView *view in self.contentView.subviews) {
        
        [view removeFromSuperview];
        
    }
    
    self.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    if ([row isKindOfClass:[L2CListDefultRow class]]) {
        
        [self.contentView addSubview:self.rowView];
        
        self.rowView.row = (L2CListDefultRow *)row;
        
        self.rowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
    }
    else if ([row isKindOfClass:[L2CListArrowRow class]]) {

        [self.contentView addSubview:self.arrowView];
        
        L2CListArrowRow *arrowRow = (L2CListArrowRow *)row;
        
        self.arrowView.row = arrowRow;
        
        self.arrowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
    }
    
    else if ([row isKindOfClass:[L2CListSwitchRow class]]) {
        
        [self.contentView addSubview:self.switchView];
        
        L2CListSwitchRow *switchRow = (L2CListSwitchRow *)row;

        self.switchView.row = switchRow;
        
        self.switchView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
    }
    
    else if ([row isKindOfClass:[L2CListTextFieldRow class]]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.textFieldView];
      
        L2CListTextFieldRow *textFieldRow = (L2CListTextFieldRow *)row;
        
        self.textFieldView.row = textFieldRow;
        
      self.textFieldView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
        
    }
    
    else if ([row isKindOfClass:[l2CListSelectBtnRow class]]) {
        
        [self.contentView addSubview:self.btnRowView];
        
        l2CListSelectBtnRow *btnRow = (l2CListSelectBtnRow *)row;
        
        self.btnRowView.row = btnRow;
        
        self.btnRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
        
    }
    else if ([row isKindOfClass:[L2CListSelectBtnAndArrowRow class]]) {
        
        [self.contentView addSubview:self.btnAndArrowRowView];
       
        L2CListSelectBtnAndArrowRow *btnRow = (L2CListSelectBtnAndArrowRow *)row;
        
        self.btnAndArrowRowView.row = btnRow;
        
        self.btnAndArrowRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
        
    }
    else if ([row isKindOfClass:[L2CListIconRow class]]) {
        
        [self.contentView addSubview:self.iconRowView];
     
        L2CListIconRow *iconRow = (L2CListIconRow *)row;
        
        self.iconRowView.row = iconRow;
        
        self.iconRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
        
    }
    else if ([row isKindOfClass:[L2CListTextViewRow class]]) {
        
        [self.contentView addSubview:self.textViewRowView];
       
        L2CListTextViewRow *textViewRow = (L2CListTextViewRow *)row;
        
        self.textViewRowView.row = textViewRow;
        
         self.textViewRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
        
    }
    else if ([row isKindOfClass:[L2CListStarViewRow class]]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.starViewRowView];
      
        L2CListStarViewRow *startViewRow = (L2CListStarViewRow *)row;
        
        self.starViewRowView.row = startViewRow;
        
        self.starViewRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
        
    }
    else if ([row isKindOfClass:[L2CListContentViewRow class]]) {
        
        [self.contentView addSubview:self.contentViewRowView];
        L2CListContentViewRow *contentViewRow = (L2CListContentViewRow *)row;
        
        contentViewRow.rowHeight = contentViewRow.titleH + contentViewRow.contentH + contentViewRow.conT + contentViewRow.conB;

        self.contentViewRowView.row = contentViewRow;
        
        self.contentViewRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
        
    }
    else if ([row isKindOfClass:[L2CListTwoTextFiledRow class]]) {
        
        [self.contentView addSubview:self.twoTextFieldRowView];
       
        L2CListTwoTextFiledRow *twoTextFiledRow = (L2CListTwoTextFiledRow *)row;
        
        self.twoTextFieldRowView.row = twoTextFiledRow;
        
        self.twoTextFieldRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
        
    }
    else if ([row isKindOfClass:[L2CListMultiTextRow class]]){
        
        [self.contentView addSubview:self.multiRowTextView];
       
        L2CListMultiTextRow *multiRowTextRow = (L2CListMultiTextRow *)row;
        
        self.multiRowTextView.row = multiRowTextRow;
        
        self.multiRowTextView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
    }
    else if ([row isKindOfClass:[L2CListButtonArrayRow class]]){
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.buttonArrayView];
        
        L2CListButtonArrayRow *buttonArrayRow = (L2CListButtonArrayRow *)row;
        
        self.buttonArrayView.row = buttonArrayRow;
        
        self.buttonArrayView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
    }
    
    else if ([row isKindOfClass:[L2CListIndicatorRow class]]){
        
        [self.contentView addSubview:self.indicatorRowView];

        self.indicatorRowView.row = (L2CListIndicatorRow *)row;
        
        self.indicatorRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
    }
    else if ([row isKindOfClass:[L2CListCustomRow class]]){
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        L2CListCustomRow *customRow = (L2CListCustomRow *)row;
     
        if (self.customView.class != customRow.clazz) {
            
            self.customView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(customRow.clazz) owner:nil options:nil].firstObject;
        
        }
        UIView *cutomView;
        
        if ([self.customView isKindOfClass:[UITableViewCell class]]){
            
            UITableViewCell *cell = (UITableViewCell *)self.customView;
            
             cutomView = cell.contentView;

             [self.contentView addSubview:cell.contentView];

        }
        else {
        
               cutomView = self.customView;
            
              [self.contentView addSubview:self.customView];

        }
        
        if (self.customView && customRow.refreshCustomBlock) {
            
           customRow.refreshCustomBlock(self.customView,customRow);
            
            customRow.customView = self.customView;
          }
        
          cutomView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
  
    }
    else if ([row isKindOfClass:[L2CListWebViewRow class]]){
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.webViewRowView];
        
        L2CListWebViewRow *webViewRow = (L2CListWebViewRow *)row;
        
        self.webViewRowView.row = webViewRow;
        
        self.webViewRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
    }else if ([row isKindOfClass:[L2CListLineInfoRow class]]){
        [self.contentView addSubview:self.lineinfoRowView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        L2CListLineInfoRow *lineInfoRow = (L2CListLineInfoRow *)row;
//        row.showSeparator = lineInfoRow.isHideLine;
        self.lineinfoRowView.row = lineInfoRow;
        
        self.lineinfoRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
    }else if ([row isKindOfClass:[L2CListGradeInfoRow class]]){
        [self.contentView addSubview:self.gradeInfoRowView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        L2CListGradeInfoRow *gradeInfoRow = (L2CListGradeInfoRow *)row;
        
        self.gradeInfoRowView.row = gradeInfoRow;
        
        self.gradeInfoRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
    }
    else if ([row isKindOfClass:[L2CListProgressRow class]]){
        
        [self.contentView addSubview:self.progressRowView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        L2CListProgressRow *progressRow = (L2CListProgressRow *)row;
        
        self.progressRowView.row = progressRow;
        
        self.progressRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
        
    }
    else if ([row isKindOfClass:[L2CListTagLabelsRow class]]){
        
        
        [self.contentView addSubview:self.tagLabelsRowView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        L2CListTagLabelsRow *progressRow = (L2CListTagLabelsRow *)row;
        
        self.tagLabelsRowView.row = progressRow;
        
        self.tagLabelsRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
       
    }
    else if ([row isKindOfClass:[L2CListSalaryRow class]]){
        [self.contentView addSubview:self.salaryRowView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        L2CListSalaryRow *lineInfoRow = (L2CListSalaryRow *)row;
        row.showSeparator = NO;
        self.salaryRowView.row = lineInfoRow;
        
        self.salaryRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
    }
    else if ([row isKindOfClass:[L2CIndContentRow class]]){
        
        [self.contentView addSubview:self.indContentView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        L2CIndContentRow *indRow = (L2CIndContentRow *)row;
        row.showSeparator = !indRow.isShowLine;
        self.indContentView.row = indRow;
        
        self.indContentView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
    }
    else if ([row isKindOfClass:[L2CListEchartsListRow class]]){
        
        [self.contentView addSubview:self.chartsRowView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        L2CListEchartsListRow *chartRow = (L2CListEchartsListRow *)row;
        
        self.chartsRowView.row = chartRow;

        self.chartsRowView.frame = CGRectMake(0, 0, kWidth, row.rowHeight?row.rowHeight:57);
    }
    
    
    if (row.isShowCheck) {
        self.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        self.accessoryType = UITableViewCellAccessoryNone;
    }

}

- (CGFloat)fetchRowHeightWithStr:(NSString *)str {

    if (!str||([str isEqualToString:@""])) {
//        如果没有内容视图那么间距也应该减去
        return 0;
    }
    
    CGFloat height = ceil([str sizeWithFont:[UIFont systemFontOfSize:16.0] and:CGSizeMake(kWidth-42, MAXFLOAT)].height) + 25;
    
    return height;
}

+ (CGFloat )fetchTitleLableWidthWithStr:(NSString *)str {
    
    CGSize size = [str boundingRectWithSize:CGSizeMake(180, 100 ) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0]} context:nil].size;
    
    return size.width+1;
}


@end
