//
//  L2CListSalaryRowView.h
//  L2Cplat
//
//  Created by liyang@l2cplat.com on 2017/11/7.
//  Copyright © 2017年 feaonline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "L2CListRowCell.h"

@interface L2CListSalaryRowView : UIView

/** 行模型 */
@property (nonatomic, strong) L2CListSalaryRow *row;
@end
